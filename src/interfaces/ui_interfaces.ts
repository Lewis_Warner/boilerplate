
import { Direction, Theme } from "@material-ui/core";

export interface ISettings {
    direction: Direction;
    responsiveFontSizes: true;
    theme: string;
}

export interface ICustomTheme extends Theme {
    name: string;
}

export interface INavItemSub {
    href: string;
    title: string;
}

export interface INavItem {
    title: string;
    icon?: any;
    href: string;
    items?: INavItemSub[];
    info?: () => JSX.Element;
}

export interface INavConfig {
    subheader: string;
    href?: string;
    items: INavItem[];
}

export interface IMenuItem {
    iconClass?: "iconError" | "iconWarn" | "iconSuccess",
    icon: any,
    text: string,
    onClick: () => void,
}
