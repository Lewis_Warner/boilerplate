import { IAppState } from "@warnster/shared-library";

export interface IUiState {
    chatSidebarOpen: boolean;
    tab: number
}

export interface IWebAppState extends IAppState {
    ui: IUiState
}