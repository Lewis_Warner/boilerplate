import { IMessageGroup, IProfile } from '@warnster/shared-interfaces';
import { secureHttp } from "@warnster/shared-library";


export const addUsersToGroup = async (group: IMessageGroup, users: IProfile[]) => {
  try {
    const { data } = await secureHttp.post("/api/groups/add-group-users", { group, users });
    return data.data;
  } catch (error) {
    console.error(error);
    return [];
  }
}
