import { UserDao } from '@warnster/shared-library';
import * as admin from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';

export class UserDaoAdmin extends UserDao {
    constructor() {
        super("admin");
        //@ts-ignore
        this.firebase = admin;
        if (!admin.apps.length) {
            if (!admin.apps.length) {
                const serviceAccount: ServiceAccount = {
                    //type: process.env.FIREBASE_ADMIN_TYPE,
                    projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
                    //private_key_id: process.env.FIREBASE_ADMIN_PRIVATE_KEY_ID,
                    privateKey: process.env.FIREBASE_ADMIN_PRIVATE_KEY,
                    clientEmail: process.env.FIREBASE_ADMIN_CLIENT_EMAIL,
                    //client_id: process.env.FIREBASE_ADMIN_CLIENT_ID,
                    //auth_uri: process.env.FIREBASE_ADMIN_AUTH_URI,
                    //token_uri: process.env.FIREBASE_ADMIN_TOKEN_URI,
                    //auth_provider_x509_cert_url: process.env.FIREBASE_ADMIN_CERTIFICATE_PROV_URL,
                    //client_x509_cert_url: process.env.FIREBASE_ADMIN_CERTIFICATE_URL
                };
                admin.initializeApp({
                    credential: admin.credential.cert(serviceAccount),
                    databaseURL: process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
                });
            }
        }
    }

    private getFirebase() {
        return <typeof admin><unknown>this.firebase;
    }

    private getFirestore() {
        return this.getFirebase().firestore();
    }

    public verifyIdToken(token: string) {
        return this.getFirebase().auth().verifyIdToken(token)
            .catch((error) => {
                console.error({ error });
                const code = error.code;
                if (error.code == "auth/id-token-expired") {
                    //reset token
                }
                throw error;
            });
    }
}