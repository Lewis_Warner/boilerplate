import { IAuthCookie } from "@warnster/shared-interfaces";
import cookie from "js-cookie";

type TAuthCookieKeys = "uid" | "email" | "token" | "emailVerified";

export const isLoggedIn = (): boolean => {
  if (getUserFromCookie()) {
    return true;
  }
  return false;
};

export const setAuthCookieValue = (key: TAuthCookieKeys, value: any): void => {
  const user = getUserFromCookie();
  //@ts-ignore
  user[key] = value;
  setAuthCookie(user);
};

export const getUserUid = (): string => {
  return getUserFromCookie().uid;
};

export const setAuthCookie = (value: IAuthCookie): void => {
  cookie.set("auth", value, {
    expires: 1,
  });
};

export const getUserFromCookie = (): IAuthCookie => {
  return cookie.getJSON("auth");
};

export const getTokenFromCookie = (): string => {
  return getUserFromCookie()?.token;
};

export const deleteAuthCookie = () => {
  cookie.remove("auth");
};
