//import { LOGOUT } from '@warnster/shared-library/dist/store/actions/accountActions';
import { accountReducer, chatReducer } from '@warnster/shared-library';
import { HYDRATE } from "next-redux-wrapper";
import { combineReducers, Reducer } from "redux";
import storage from "redux-persist/lib/storage";
import uiReducer from './uiReducer';

const appReducer: Reducer = combineReducers({
  chat: chatReducer,
  account: accountReducer,
  ui: uiReducer
});

const rootReducer = (state: any, action: any) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    }
    return nextState
  } else {
    // when a logout action is dispatched it will reset redux state and clear redux persist
    if (action.type === "@account/logout") {
      storage.removeItem("persist:nextjs");
      state = undefined;
    }

    return appReducer(state, action);
  }
};

export default rootReducer;
