import { produce } from "immer";
import { IUiState } from "../../interfaces/state_interfaces";
import { CLOSE_SIDEBAR, OPEN_SIDEBAR, SET_BOTTOM_MAIN_TAB } from "../actions/uiActions";

export const uiInitialState: IUiState = {
    chatSidebarOpen: false,
    tab: 0
};

const uiReducer = (state = uiInitialState, action: any) => {
    switch (action.type) {

        case OPEN_SIDEBAR: {
            return produce(state, (draft) => {
                draft.chatSidebarOpen = true;
            });
        }

        case CLOSE_SIDEBAR: {
            return produce(state, (draft) => {
                draft.chatSidebarOpen = false;
            });
        }

        case SET_BOTTOM_MAIN_TAB: {
            return produce(state, (draft) => {
                draft.tab = action.payload.tab
            })
        }

        default: {
            return state;
        }
    }
}


export default uiReducer;