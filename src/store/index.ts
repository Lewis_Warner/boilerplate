import { userInitialState } from "@warnster/shared-library/dist/store/reducers/accountReducer";
import { chatInitialState } from "@warnster/shared-library/dist/store/reducers/chatReducer";
import { useMemo } from "react";
import {
  applyMiddleware,
  compose,
  createStore,
  Store,
  StoreEnhancer
} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
// @ts-ignore
import { createLogger } from "redux-logger";
import thunkMiddleware from "redux-thunk";
import { IWebAppState } from "../interfaces/state_interfaces";
import rootReducer from "./reducers";
import { uiInitialState } from "./reducers/uiReducer";
export const initialState: IWebAppState = {
  account: userInitialState,
  chat: chatInitialState,
  ui: uiInitialState
};

const loggerMiddleware = createLogger();

let store: Store | undefined;

function initStore(
  preloadedState: IWebAppState = initialState,
  middleware: StoreEnhancer
) {
  return createStore(rootReducer, preloadedState, middleware);
}

export const initializeStore = (preloadedState?: IWebAppState) => {
  const middlewares = [thunkMiddleware];

  if (process.env.ENABLE_REDUX_LOGGER) {
    middlewares.push(loggerMiddleware);
  }

  const middlewareEnhancer = composeWithDevTools(
    applyMiddleware(...middlewares)
  );

  const enhancers = [middlewareEnhancer];
  const composedEnhancers: StoreEnhancer = compose(...enhancers);
  let _store = store ?? initStore(preloadedState, composedEnhancers);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore(
      {
        ...store.getState(),
        ...preloadedState,
      },
      composedEnhancers
    );
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState: any): Store {
  const store: Store = useMemo(() => initializeStore(initialState), [
    initialState,
  ]);
  return store;
}
