export const OPEN_SIDEBAR = "@chat/open-sidebar";
export const CLOSE_SIDEBAR = "@chat/close-sidebar";
export const SET_BOTTOM_MAIN_TAB = "@account/set-tab"

export function openSidebar() {
    return {
        type: OPEN_SIDEBAR,
    };
}

export function closeSidebar() {
    return {
        type: CLOSE_SIDEBAR,
    };
}

export function setTab(tab: number) {
    return {
        type: SET_BOTTOM_MAIN_TAB,
        payload: { tab }
    }
}
