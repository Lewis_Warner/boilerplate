import { Badge, BottomNavigation, BottomNavigationAction, Divider, Hidden, makeStyles } from "@material-ui/core";
import { ChatHelper } from "@warnster/shared-library";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import {
  Home,
  MessageCircle as MessageCircleIcon,
  Users as UsersIcon
} from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IWebAppState } from "../../interfaces/state_interfaces";
import { setTab } from "../../store/actions/uiActions";
import AdminNavBar from "./NavBar/admin_nav_bar";
import NavBar from "./NavBar/nav_bar";
import TopBar from "./TopBar/top_nav";


const useStyles = makeStyles((theme) => ({
  root: {
    //@ts-ignore
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    overflow: "hidden",
    width: "100%",
  },
  wrapper: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
    paddingTop: 64,
    [theme.breakpoints.down('sm')]: {
      paddingBottom: 57,
    },
    [theme.breakpoints.up("lg")]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
  },
  content: {
    flex: "1 1 auto",
    height: "100%",
    overflow: "auto",
  },
  stickToBottom: {
    width: '100%',
    position: 'fixed',
    bottom: 0,
  },
}));

const withDashboardLayout = (ComposedComponent: any) => {
  function DashboardLayout(props: any) {
    const classes = useStyles();
    const [isMobileNavOpen, setMobileNavOpen] = useState(false);
    const dispatch = useDispatch();
    const { account, chat, ui } = useSelector((state: IWebAppState) => state);
    const { user } = account;
    const { tab } = ui;
    const router = useRouter();

    const tabNavigation: string[] = [
      '/feed',
      '/profiles',
      '/chat',
    ]

    const getChatIcon = () => {
      const unread = ChatHelper.calculateTotalUnreadMessages(chat.groups, user.uid);
      if (unread > 0) {
        return <Badge badgeContent={unread} color="primary"><MessageCircleIcon /></Badge>;
      }
      return (<MessageCircleIcon />)
    }


    useEffect(() => {
      if (router.pathname === '/chat') {
        dispatch(setTab(2))
      } else if (router.pathname === '/profiles') {
        dispatch(setTab(1))
      } else if (router.pathname === '/feed') {
        dispatch(setTab(0))
      } else {
        dispatch(setTab(-1))
      }
    }, [router.pathname])

    return (
      <div className={classes.root}>
        <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} />

        {user.isAdmin ? (
          <AdminNavBar
            onMobileClose={() => setMobileNavOpen(false)}
            openMobile={isMobileNavOpen}
          />
        ) : (
          <NavBar
            onMobileClose={() => setMobileNavOpen(false)}
            openMobile={isMobileNavOpen}
          />
        )}

        <div className={classes.wrapper}>
          <div className={classes.contentContainer}>
            <div className={classes.content}>
              <ComposedComponent {...props} />
            </div>
          </div>
        </div>

        <Hidden mdUp>
          <div
            className={classes.stickToBottom}>
            <Divider
            />
            <BottomNavigation
              value={tab}
              onChange={(event, newValue) => {
                dispatch(setTab(newValue))
                router.push(tabNavigation[newValue])
              }}
              showLabels
            >
              <BottomNavigationAction label="Home" icon={<Home />} />
              <BottomNavigationAction label="Profiles" icon={<UsersIcon />} />
              <BottomNavigationAction label="Chats" icon={getChatIcon()} />
            </BottomNavigation>
          </div>
        </Hidden>
      </div>
    );
  }

  return DashboardLayout;
};

export default withDashboardLayout;
