//import { useDispatch, useSelector } from 'react-redux';
//import { useSnackbar } from 'notistack';
import {
  Avatar,
  Box,
  ButtonBase,
  Hidden,
  makeStyles,
  Menu,
  MenuItem,
  Typography
} from "@material-ui/core";
import { AccountActions, IAppState } from '@warnster/shared-library';
import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteAuthCookie } from "../../../store/persistant/cookieStore";
//import { logout } from 'src/actions/accountActions';

const useStyles = makeStyles((theme) => ({
  avatar: {
    height: 32,
    width: 32,
    marginRight: theme.spacing(1),
  },
  popover: {
    width: 200,
  },
}));

function Account() {
  const classes = useStyles();
  const router = useRouter();
  const ref = useRef(null);
  const { user, profile } = useSelector((state: IAppState) => state.account);
  const dispatch = useDispatch();
  //const account = useSelector((state) => state.account);
  //const { enqueueSnackbar } = useSnackbar();
  const [isOpen, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const logoutHandler = () => {
    router.push("/login");
    dispatch(AccountActions.logout());
    deleteAuthCookie();
  };

  const account = {
    user: {
      avatar: profile.avatar,
      name: profile.name,
    },
  };

  return (
    <div>
      <Box
        display="flex"
        alignItems="center"
        component={ButtonBase}
        onClick={handleOpen}
        {...{ ref }}
      >
        <Avatar
          alt="User"
          className={classes.avatar}
          // @ts-ignore
          src={account.user.avatar}
        />
        <Hidden smDown>
          <Typography variant="h6" color="inherit">
            {account.user.name}
          </Typography>
        </Hidden>
      </Box>
      <Menu
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        keepMounted
        PaperProps={{ className: classes.popover }}
        getContentAnchorEl={null}
        anchorEl={ref.current}
        open={isOpen}
      >
        <MenuItem onClick={() => { router.push('/account') }}>Account</MenuItem>
        <MenuItem onClick={() => logoutHandler()}>Logout</MenuItem>
      </Menu>
    </div>
  );
}

export default Account;
