import {
  AppBar,
  Box,
  Hidden,
  IconButton,

  makeStyles,
  SvgIcon, Toolbar
} from "@material-ui/core";
import clsx from "clsx";
import Link from "next/link";
import PropTypes from "prop-types";
import React from "react";
import { Menu as MenuIcon } from "react-feather";
import Logo from "../../../components/Logo";
import { ICustomTheme } from "../../../interfaces/ui_interfaces";
import { THEMES } from "../../../theme/theme";
import Account from "./Account";

const useStyles = makeStyles((theme: ICustomTheme) => ({
  root: {
    zIndex: theme.zIndex.drawer + 100,
    ...(theme.name === THEMES.LIGHT
      ? {
        boxShadow: "none",
        backgroundColor: theme.palette.primary.main,
      }
      : {}),
    ...(theme.name === THEMES.ONE_DARK
      ? {
        backgroundColor: theme.palette.background.default,
      }
      : {}),
  },
  toolbar: {
    minHeight: 64,
  },
  menuButton: {},
}));

interface TopBarPropInterface {
  className?: string;
  onMobileNavOpen: () => void;
}

function TopBar({ className, onMobileNavOpen, ...rest }: TopBarPropInterface) {
  const classes = useStyles();

  return (
    <AppBar className={clsx(classes.root, className)} {...rest}>
      <Toolbar className={classes.toolbar}>
        <Hidden lgUp>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            onClick={onMobileNavOpen}
          >
            <SvgIcon fontSize="small">
              <MenuIcon />
            </SvgIcon>
          </IconButton>
        </Hidden>
        <Hidden mdDown>
          <Link href="/">
            <Logo />
          </Link>
        </Hidden>
        <Box ml={2} flexGrow={1} />
        <Box ml={2}>
          <Account />
        </Box>
      </Toolbar>
    </AppBar>
  );
}

TopBar.propTypes = {
  className: PropTypes.string,
  onMobileNavOpen: PropTypes.func,
};

export default TopBar;
