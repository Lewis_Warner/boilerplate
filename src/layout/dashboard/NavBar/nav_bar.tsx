/* eslint-disable no-use-before-define */
/* eslint-disable react/prop-types */
import {
  Avatar,
  Box,
  Chip,
  Divider,
  Drawer,
  Hidden,
  Link,
  List,
  ListSubheader,
  makeStyles,
  Typography
} from "@material-ui/core";
import { ChatHelper, IAppState } from "@warnster/shared-library";
import NextLink from "next/link";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { Home, MessageCircle, Users } from "react-feather";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useSelector } from "react-redux";
import Logo from "../../../../src/components/Logo";
import { INavConfig, INavItem } from "../../../interfaces/ui_interfaces";
import NavItem from "./NavItem";

interface renderNavItemsProp {
  items: INavItem[];
  depth?: number;
}

function renderNavItems({ items, depth = 0 }: renderNavItemsProp) {
  return (
    <List disablePadding>
      {items.reduce((acc, item) => reduceChildRoutes({ acc, item, depth }), [])}
    </List>
  );
}

interface reduceChildRoutesProp {
  item: INavItem;
  acc: any;
  depth: number;
}

function reduceChildRoutes({ acc, item, depth = 0 }: reduceChildRoutesProp) {
  const key = item.title + depth;

  if (item.items) {
    acc.push(
      <NavItem
        depth={depth}
        icon={item.icon}
        key={key}
        info={item.info}
        open={false}
        title={item.title}

      >
        {renderNavItems({
          depth: depth + 1,
          items: item.items,
        })}
      </NavItem>
    );
  } else {
    acc.push(
      <NextLink passHref key={key} href={item.href}>
        <NavItem
          open={false}
          depth={depth}
          icon={item.icon}
          info={item.info}
          title={item.title}
        />
      </NextLink>
    );
  }

  return acc;
}

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: "calc(100% - 64px)",
  },
  avatar: {
    cursor: "pointer",
    width: 64,
    height: 64,
  },
}));

function NavBar({ openMobile, onMobileClose }: any) {
  const classes = useStyles();
  const { account, chat } = useSelector((state: IAppState) => state);
  const { user, profile } = account;
  const { groups } = chat;

  const navConfig: INavConfig[] = [
    {
      subheader: "Applications",
      items: [
        {
          title: "Home",
          href: "/feed",
          icon: Home,
        },
        {
          title: "Profiles",
          href: "/profiles",
          icon: Users,
        },
        {
          title: "Chat",
          href: "/chat",
          icon: MessageCircle,
          info: () => {
            const unread = ChatHelper.calculateTotalUnreadMessages(groups, user.uid);
            if (unread > 0) {
              return <Chip color="secondary" size="small" label={unread} />;
            }
            return <div></div>;
          },
        },
      ],
    },
  ];

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line
  }, []);

  const content = !user ? (
    <h1>Loading</h1>
  ) : (
    <Box height="100%" display="flex" flexDirection="column">
      <PerfectScrollbar options={{ suppressScrollX: true }}>
        <Hidden lgUp>
          <Box p={2} display="flex" justifyContent="center">
            <Link href="/">
              <Logo />
            </Link>
          </Box>
        </Hidden>
        <Box p={2}>
          <Box display="flex" justifyContent="center">
            <Link href="/app/account">
              <Avatar alt="User" className={classes.avatar} src={profile.avatar} />
            </Link>
          </Box>
          <Box mt={2} textAlign="center">
            <Link
              href="/app/account"
              variant="h5"
              color="textPrimary"
              underline="none"
            >
              {profile.name}
            </Link>
            <Typography variant="body2" color="textSecondary">
              This is my amazing BIO
            </Typography>
          </Box>
        </Box>
        <Divider />
        <Box p={2}>
          {navConfig.map((config) => (
            <List
              key={config.subheader}
              subheader={
                <ListSubheader disableGutters disableSticky>
                  {config.subheader}
                </ListSubheader>
              }
            >
              {renderNavItems({ items: config.items })}
            </List>
          ))}
        </Box>
      </PerfectScrollbar>
    </Box>
  );
  return (
    <div>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          classes={{ paper: classes.desktopDrawer }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
    </div>
  );
}

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

export default NavBar;
