import ReactGA from 'react-ga';

export const GA_TRACKING_ID = "UA-193343152-1";
ReactGA.initialize(GA_TRACKING_ID);

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url: string) => {
    ReactGA.pageview(url);
};



// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = (event: ReactGA.EventArgs) => {
    ReactGA.event(event);
};