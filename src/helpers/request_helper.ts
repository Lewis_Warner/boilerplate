import { IFirebaseJWT } from "@warnster/shared-interfaces";
import { NextApiRequest } from "next";
import { UserDaoAdmin } from "../daos/user_admin_dao";

export const checkRequestIsAuthorised = async (
    req: NextApiRequest
): Promise<IFirebaseJWT | false> => {
    const token = <string>req.headers["x-access-token"];
    try {
        const userDao = new UserDaoAdmin();
        return <IFirebaseJWT>await userDao.verifyIdToken(token);
    } catch (err) {
        return false;
    }
};