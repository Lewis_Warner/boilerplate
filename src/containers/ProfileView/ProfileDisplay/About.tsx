import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  colors, Divider,
  LinearProgress,
  Link,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles, Typography
} from '@material-ui/core';
import { IProfile } from '@warnster/shared-interfaces';
import clsx from 'clsx';
import React from 'react';
import {
  Briefcase as BriefcaseIcon, Home as HomeIcon,
  Mail as MailIcon, Plus as PlusIcon
} from 'react-feather';

const useStyles = makeStyles((theme) => ({
  root: {},
  jobAvatar: {
    backgroundColor: theme.palette.secondary.main
  },
  cityAvatar: {
    backgroundColor: colors.red[600]
  }
}));

interface IAboutProps {
  className?: string,
  profile: IProfile
}

function About({
  className,
  profile,
  ...rest
}: IAboutProps) {
  const classes = useStyles();

  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card>
        <CardHeader title="Profile Progress" />
        <Divider />
        <CardContent>
          <LinearProgress
            variant="determinate"
            value={50}
          />
          <Box mt={2}>
            <Typography
              variant="subtitle2"
              color="textSecondary"
            >
              50% Set Up Complete
            </Typography>
          </Box>
        </CardContent>
      </Card>
      <Box mt={3}>
        <Card>
          <CardHeader title="About" />
          <Divider />
          <CardContent>
            <Typography
              variant="subtitle2"
              color="textSecondary"
            >
              &quot;
              {profile.name}
              &quot;
            </Typography>
            <List>
              <ListItem
                disableGutters
                divider
              >
                <ListItemAvatar>
                  <Avatar className={classes.jobAvatar}>
                    <BriefcaseIcon size="22" />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  disableTypography
                  primary={(
                    <Typography
                      variant="body2"
                      color="textPrimary"
                    >
                      Top Developer
                      {' '}
                      at
                      {' '}
                      <Link
                        variant="subtitle2"
                        color="textPrimary"
                        href="#"
                      >
                        Billing co
                      </Link>
                    </Typography>
                  )}
                  secondary={(
                    <Typography
                      variant="caption"
                      color="textSecondary"
                    >
                      Past:
                      "Software Engineer"
                      {' '}
                      <Link
                        variant="caption"
                        color="textSecondary"
                        href="#"
                      >
                        "Ethixbase Base"
                      </Link>
                    </Typography>
                  )}
                />
              </ListItem>
              <ListItem
                disableGutters
                divider
              >
                <ListItemAvatar>
                  <Avatar className={classes.cityAvatar}>
                    <PlusIcon size="22" />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary="Add school or collage"
                  primaryTypographyProps={{
                    variant: 'body2',
                    color: 'textSecondary'
                  }}
                />
              </ListItem>
              <ListItem
                disableGutters
                divider
              >
                <ListItemAvatar>
                  <Avatar className={classes.cityAvatar}>
                    <HomeIcon size="22" />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  disableTypography
                  primary={(
                    <Typography
                      variant="body2"
                      color="textPrimary"
                    >
                      Lives in
                      {' '}
                      <Link
                        variant="subtitle2"
                        color="textPrimary"
                        href="#"
                      >
                        United Kingdom
                      </Link>
                    </Typography>
                  )}
                />
              </ListItem>
              <ListItem
                disableGutters
                divider
              >
                <ListItemAvatar>
                  <Avatar className={classes.cityAvatar}>
                    <MailIcon size="22" />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={"lawdwad@awdwa.com"}
                  primaryTypographyProps={{
                    variant: 'body2',
                    color: 'textPrimary'
                  }}
                />
              </ListItem>
            </List>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
}


export default About;
