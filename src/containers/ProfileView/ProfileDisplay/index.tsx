import {
  Box,



  Grid,
  makeStyles
} from '@material-ui/core';
import { IPostFilter, IProfile } from '@warnster/shared-interfaces';
import { usePosts } from '@warnster/shared-library';
import clsx from 'clsx';
import React from 'react';
import { useSelector } from 'react-redux';
import PostAdd from '../../../components/Posts/PostAdd';
import PostCard from '../../../components/Posts/PostCard';
import { IWebAppState } from '../../../interfaces/state_interfaces';

const useStyles = makeStyles(() => ({
  root: {}
}));

interface IProfileDisplayProps {
  profile: IProfile,
  className?: string
}

function ProfileDisplay({
  className,
  profile,
  ...rest
}: IProfileDisplayProps) {
  const classes = useStyles();
  const { profile: profileRedux } = useSelector((state: IWebAppState) => state.account)
  const isUserProfile = profile.profileID === profileRedux.profileID
  const postFilter: IPostFilter = {
    postAuthorID: profile.profileID,
    limit: 20,
  }
  const { posts } = usePosts(postFilter);
  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid
        container
        spacing={3}
      >
        <Grid
          item
          xs={12}
          md={12}
          lg={8}
        >
          {isUserProfile ?

            <Box>
              <PostAdd />
            </Box> :
            ''
          }
          {
            posts.map((post, index) => (
              <Box
                mt={3}
                key={index}
              >
                <PostCard post={post} />
              </Box>
            ))

          }
        </Grid>
      </Grid>
    </div>
  );
}

export default ProfileDisplay;
