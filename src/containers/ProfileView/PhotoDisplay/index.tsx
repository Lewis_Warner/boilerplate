import {
    Card,
    CardContent,
    CardHeader,
    Grid,
    makeStyles
} from '@material-ui/core';
import { IProfile } from '@warnster/shared-interfaces';
import clsx from 'clsx';
import React from 'react';
import ImageList from '../../../components/Images/ImageList';

const useStyles = makeStyles(() => ({
    root: {}
}));

interface IPhotoDisplayProps {
    profile: IProfile,
    className?: string
}

function PhotoDisplay({
    className,
    profile,
    ...rest
}: IPhotoDisplayProps) {
    const classes = useStyles();

    return (
        <div
            className={clsx(classes.root, className)}
            {...rest}
        >
            <Grid
                container
                spacing={3}
            >
                <Grid
                    item
                    xs={12}
                    md={12}
                    lg={12}
                >
                    <Card>
                        <CardHeader title="Photos"></CardHeader>
                        <CardContent>
                            <ImageList images={profile.images} />

                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
}

export default PhotoDisplay;
