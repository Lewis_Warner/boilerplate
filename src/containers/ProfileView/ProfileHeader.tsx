import {
  Avatar,
  Box,






  colors, Container,





  makeStyles, Typography
} from '@material-ui/core';
import { IProfile } from '@warnster/shared-interfaces';
import clsx from 'clsx';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  root: {},
  cover: {
    position: 'relative',
    height: 250,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    '&:before': {
      position: 'absolute',
      content: '" "',
      top: 0,
      left: 0,
      height: '100%',
      width: '100%',
      backgroundImage: 'linear-gradient(-180deg, rgba(0,0,0,0.00) 58%, rgba(0,0,0,0.32) 100%)'
    },
    '&:hover': {
      '& $changeButton': {
        visibility: 'visible'
      }
    }
  },
  changeButton: {
    visibility: 'hidden',
    position: 'absolute',
    bottom: theme.spacing(3),
    right: theme.spacing(3),
    backgroundColor: colors.blueGrey[900],
    color: theme.palette.common.white,
    [theme.breakpoints.down('md')]: {
      top: theme.spacing(3),
      bottom: 'auto'
    },
    '&:hover': {
      backgroundColor: colors.blueGrey[900]
    }
  },
  addPhotoIcon: {
    marginRight: theme.spacing(1)
  },
  avatar: {
    border: `2px solid ${theme.palette.common.white}`,
    height: 120,
    width: 120,
    top: -60,
    left: theme.spacing(0),
    position: 'absolute'
  },
  action: {
    marginLeft: theme.spacing(1)
  }
}));

interface IProfileHeaderProps {
  className?: string,
  profile: IProfile
}

function ProfileHeader({
  className,
  profile,
  ...rest
}: IProfileHeaderProps) {
  const classes = useStyles();

  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <div
        className={classes.cover}
        style={{ backgroundImage: `url(${profile.coverPhoto})` }}
      >
      </div>
      <Container maxWidth="lg">
        <Box
          position="relative"
          mt={1}
          display="flex"
          alignItems="center"
        >
          <Avatar
            alt="Person"
            className={classes.avatar}
            src={profile.avatar}
          />
          <Box marginLeft="136px">
            <Typography
              variant="overline"
              color="textSecondary"
            >
              Online
            </Typography>
            <Typography
              variant="h4"
              color="textPrimary"
            >
              {profile.name}
            </Typography>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default ProfileHeader;
