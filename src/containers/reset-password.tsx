import { Box, Button, makeStyles, TextField } from "@material-ui/core";
import { UserDaoClient } from "@warnster/shared-library";
import { Formik } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import React from "react";
import * as Yup from "yup";
import Error404View from "../../pages/404";

const useStyles = makeStyles(() => ({
  root: {},
}));

function ResetPasswordForm({ className, ...rest }: { className?: string }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const router = useRouter();
  if (!router.query?.oobCode) {
    return <Error404View />;
  }

  const submit = async (
    values: any,
    {
      resetForm,
      setErrors,
      setStatus,
      setSubmitting,
    }: { resetForm: any; setErrors: any; setStatus: any; setSubmitting: any }
  ) => {
    try {
      const userDao = new UserDaoClient();
      userDao.confirmPasswordReset(
        router.query.oobCode as string,
        values.password
      );
      // Make API request
      resetForm();
      setStatus({ success: true });
      setSubmitting(false);
      enqueueSnackbar("Password Updated", {
        variant: "success",
      });
      router.push("/login");
    } catch (error) {
      setStatus({ success: false });
      setErrors({ submit: error.message });
      setSubmitting(false);
    }
  };

  return (
    <Formik
      initialValues={{
        password: "",
        passwordConfirm: "",
      }}
      validationSchema={Yup.object().shape({
        password: Yup.string()
          .min(6, "Must be at least 6 characters")
          .max(255)
          .required("Required"),
        passwordConfirm: Yup.string()
          .oneOf([Yup.ref("password"), undefined], "Passwords must match")
          .required("Required"),
      })}
      onSubmit={submit}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
      }) => (
        <form onSubmit={handleSubmit}>
          <TextField
            error={Boolean(touched.password && errors.password)}
            fullWidth
            margin="normal"
            helperText={touched.password && errors.password}
            label="Password"
            name="password"
            onBlur={handleBlur}
            onChange={handleChange}
            type="password"
            value={values.password}
            variant="outlined"
          />
          <TextField
            error={Boolean(touched.passwordConfirm && errors.passwordConfirm)}
            fullWidth
            margin="normal"
            helperText={touched.passwordConfirm && errors.passwordConfirm}
            label="Password Confirmation"
            name="passwordConfirm"
            onBlur={handleBlur}
            onChange={handleChange}
            type="password"
            value={values.passwordConfirm}
            variant="outlined"
          />
          <Box p={2} display="flex" justifyContent="flex-end">
            <Button
              color="secondary"
              disabled={isSubmitting}
              type="submit"
              variant="contained"
            >
              Change Password
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
}

ResetPasswordForm.propTypes = {
  className: PropTypes.string,
};

export default ResetPasswordForm;
