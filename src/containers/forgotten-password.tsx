import {
  Box,
  Button,


  makeStyles, TextField
} from "@material-ui/core";
import { UserDaoClient } from "@warnster/shared-library";
import clsx from "clsx";
import { Formik } from "formik";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import React from "react";
import * as Yup from "yup";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ForgottenPasswordForm = ({
  className,
  ...rest
}: {
  className?: string;
}) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const submit = async (
    values: any,
    {
      resetForm,
      setErrors,
      setStatus,
      setSubmitting,
    }: { resetForm: any; setErrors: any; setStatus: any; setSubmitting: any }
  ) => {
    try {
      const userDao = new UserDaoClient();
      userDao.sendPasswordResetEmail(values.email);
      resetForm();
      enqueueSnackbar("Email Sent", {
        variant: "success",
      });
    } catch (error) {
      setStatus({ success: false });
      setErrors({ submit: error.message });
      setSubmitting(false);
    }
  };

  return (
    <Formik
      initialValues={{
        email: "",
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email("Must be a valid email")
          .max(255)
          .required("Email is required"),
      })}
      onSubmit={submit}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <TextField
            error={Boolean(touched.email && errors.email)}
            fullWidth
            helperText={touched.email && errors.email}
            label="Email Address"
            margin="normal"
            name="email"
            onBlur={handleBlur}
            onChange={handleChange}
            type="email"
            value={values.email}
            variant="outlined"
          />
          <Box mt={2}>
            <Button
              color="secondary"
              disabled={isSubmitting}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              Send Reset Password Link
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
};
ForgottenPasswordForm.propTypes = {
  className: PropTypes.string,
};

export default ForgottenPasswordForm;
