import {
  IMessageGroup,
  IMessageGroupRetrievedFirebase,
  IUser
} from "@warnster/shared-interfaces";
import {
  AccountActions, ChatActions, ChatDaoClient,
  ChatHelper, IAppState,
  UserDaoClient
} from "@warnster/shared-library";
import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";


interface IGroupListeners { listener: () => void, groupIDs: string[] };

//This is a wrapper component which is not refreshed on local http redirects for nextjs. Global firebase listeners should be initalised here
function BaseWrapper({ children }: { children: any }) {
  const dispatch = useDispatch();

  const { account } = useSelector((state: IAppState) => state);
  const { user, loggedIn } = account;

  const onGroupSnapshot = (
    groupChunkIDs: string[]
  ) => {
    const chatDao = new ChatDaoClient();
    return chatDao.getGroups(groupChunkIDs).onSnapshot(async (querySnapshot) => {
      const groups: IMessageGroup[] = [];
      const deletedGroupIDs: string[] = [];
      for (let i = 0; i < querySnapshot.docChanges().length; i++) {
        const doc = querySnapshot.docChanges()[i]
        let tmpGroup: IMessageGroup;
        const docData = doc.doc.data();
        //ignore local update for now
        if ((doc.doc.metadata.hasPendingWrites === true && docData.id) ||
          doc.doc.metadata.hasPendingWrites === false) {
          //If doc is removed then remove the group from redux
          if (doc.type === "removed") {
            deletedGroupIDs.push(docData.id)
          } else {
            tmpGroup = await ChatHelper.transformFirebaseGroup(
              doc.doc.data() as IMessageGroupRetrievedFirebase
            );
            groups.push(tmpGroup);
          }
        } else {
          console.log("NOT ADDING AS IT IS BRAND NEW GROUP AND CAN'T BE FILTERED???")
        }

      };
      if (groups.length > 0) {
        dispatch(ChatActions.updateGroups(groups));
      }
      if (deletedGroupIDs.length > 0) {
        dispatch(ChatActions.removeGroups(deletedGroupIDs))
      }
    })
  };

  const createGroupListener = useCallback((groupIDs: string[]) => {
    const groupChunks: string[][] = [];
    //Split ids in to chunks of 10
    const chunkSize = 10;
    const loopLength = groupIDs.length / chunkSize
    for (let i = 0; i < loopLength; i++) {
      groupChunks.push(groupIDs.slice(i * chunkSize, (i * chunkSize) + chunkSize))
    }
    const listeners: IGroupListeners[] = [];
    for (let i = 0; i < groupChunks.length; i++) {
      const groupChunkIDs = groupChunks[i];
      const listener = onGroupSnapshot(groupChunkIDs);
      listeners.push({ listener, groupIDs: groupChunkIDs });
    }
    return listeners;
  }, []);


  const onUserUpdate = (updatedUser: IUser, groupListeners: IGroupListeners[]) => {

    //if group added, add a listeners for that group
    if (user.groups.length < updatedUser.groups.length) {
      //Find the new group id by searching through current user groups
      const newGroupIDs = updatedUser.groups.filter((groupID) => {
        if (!user.groups.find((gID) => groupID === gID)) {
          return groupID
        }
      })
      //adds new listeners for groupid
      if (newGroupIDs.length > 0) {
        groupListeners.push({ listener: onGroupSnapshot(newGroupIDs), groupIDs: newGroupIDs });
      }
    } else
      //group has been deleted
      if (user.groups.length > updatedUser.groups.length) {
        console.error('Group Was Delete, Not Implemented')
        //find deleted Group
        const deletedGroupIDs = user.groups.filter((groupID) => {
          if (!updatedUser.groups.find((gID) => groupID === gID)) {
            return groupID
          }
        })
        //loop through and find the listner with the groupID in that has been remove.
        dispatch(ChatActions.removeGroups(deletedGroupIDs))
        deletedGroupIDs.forEach(deletedGroupID => {
          groupListeners.forEach(listener => {
            if (listener.groupIDs.includes(deletedGroupID)) {
              //Reinitalise the listeners with the remaining group IDs
              const groupIDsForListener = listener.groupIDs.filter((gid) => gid !== deletedGroupID)
              console.log('ending previous listener and starting new listener with group id: ', { groupIDsForListener, groupIDs: listener.groupIDs })
              listener.listener();
              //if listener now has 0 ids, do not regenerate
              if (groupIDsForListener.length !== 0) {
                const newListener = onGroupSnapshot(groupIDsForListener);
                groupListeners.push({ listener: newListener, groupIDs: groupIDsForListener });
              }
            }
          })
        })
      }

    dispatch(AccountActions.setUser(updatedUser));
  }


  useEffect(() => {
    if (!user.uid) return;
    //reset groups on login or page refresh?
    dispatch(ChatActions.setGroups([]));

    let groupListeners: IGroupListeners[] = [];

    if (user.groups.length > 0) {
      groupListeners = createGroupListener(user.groups);
    } else {
      dispatch(ChatActions.updateGroupsLoaded(true))
    }
    const userDao = new UserDaoClient();
    const userListener = userDao.onUserUpdate(user.uid, (updatedUser) => {
      onUserUpdate(updatedUser, groupListeners)
    });

    return () => {
      if (groupListeners) {
        groupListeners.forEach((listener) => {
          listener.listener();
        });
      }
      if (userListener) {
        userListener();
      }
    };
  }, [loggedIn]);

  return children;
}

export default BaseWrapper;
