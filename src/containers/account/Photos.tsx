
import { Box, Card, CardContent, CardHeader, Divider, Grid, LinearProgress, makeStyles } from '@material-ui/core';
import { IImage, IProfileUpdate } from "@warnster/shared-interfaces";
import {
    AccountActions, IAppState,
    StorageDaoClient
} from '@warnster/shared-library';
import clsx from 'clsx';
import firebase from 'firebase';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FilesDropzone from '../../components/FilesDropzone';
import ImageCard from '../../components/Images/ImageCard';

const useStyles = makeStyles(() => ({
    root: {}
}));

interface IPhotosProps {
    className?: string
}

function Photos({ className, ...rest }: IPhotosProps) {
    const classes = useStyles();
    const dispatch = useDispatch()
    const [submittingFiles, setSubmittingFiles] = useState(false)
    const [uploadPercentage, setUploadPercentage] = useState(0)
    const { profile } = useSelector((state: IAppState) => state.account);
    const { enqueueSnackbar } = useSnackbar();


    const uploadFiles = async (files: File[]) => {
        const storageDao = new StorageDaoClient()
        let totalBytes = 0;
        for (let i = 0; i < files.length; i++) {
            totalBytes += files[i].size;
        }
        setSubmittingFiles(true)
        const profileUpdate: IProfileUpdate = {
            updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
        }
        const images: IImage[] = []

        let currentBytesUploaded = 0;
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            const task = await storageDao.uploadFile(file, `profiles/${profile.profileID}`, (uploadTask) => {
                currentBytesUploaded += uploadTask.bytesTransferred
                setUploadPercentage((currentBytesUploaded / totalBytes) * 100)
            })
            images.push({ path: task.ref.fullPath, url: await task.ref.getDownloadURL() });
        }
        profileUpdate.images = firebase.firestore.FieldValue.arrayUnion(...images);
        profileUpdate.imagesNew = images
        //update with redux
        if (profile.images.length === 0) {
            profileUpdate.avatar = images[0].url
        }
        dispatch(AccountActions.updateProfile(profileUpdate, profile.profileID))
        setSubmittingFiles(false)
        enqueueSnackbar("Images Uploaded", {
            variant: "success",
            anchorOrigin: {
                vertical: "top",
                horizontal: "right",
            },
            autoHideDuration: 3000,
            persist: false,
        });
    }

    return (
        <Grid
            className={clsx(classes.root, className)}
            container
            spacing={3}
            {...rest}
        >
            <Grid
                item
                lg={4}
                md={6}
                xl={3}
                xs={12}
            >
                <ImageCard />
            </Grid>
            <Grid
                item
                lg={8}
                md={6}
                xl={9}
                xs={12}
            >

                <Box >
                    <Card>
                        <CardHeader title="Upload Images" />

                        {submittingFiles ?
                            <LinearProgress variant="determinate" value={uploadPercentage} />
                            : <Divider />}
                        <CardContent>
                            <FilesDropzone isSubmitting={submittingFiles} uploadFiles={uploadFiles}></FilesDropzone>
                        </CardContent>
                    </Card>
                </Box>
            </Grid>
        </Grid>
    );
}

Photos.propTypes = {
    className: PropTypes.string
};

export default Photos;
