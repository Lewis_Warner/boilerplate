import {
  Backdrop,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,



  makeStyles, Paper,
  Typography
} from '@material-ui/core';
import { loadStripe } from '@stripe/stripe-js';
import { ICheckoutSessionItem, IPlanDetails, ISubscription } from "@warnster/shared-interfaces";
import { IAppState, PRICING_DETAILS, Stripe, UserHelper } from '@warnster/shared-library';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {},
  overview: {
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column-reverse',
      alignItems: 'flex-start'
    }
  },
  productImage: {
    marginRight: theme.spacing(1),
    height: 48,
    width: 48
  },
  details: {
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      alignItems: 'flex-start'
    }
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));



interface ISubscriptionProps {
  className?: string
}

function Subscription({ className, ...rest }: ISubscriptionProps) {
  const classes = useStyles();
  const [portalUrl, setPortalUrl] = useState<null | string>(null)
  const { user } = useSelector((state: IAppState) => state.account)
  const [hasPaidSubscription, setHasPaidSubscription] = useState(false);
  const [subscription, setSubscription] = useState<IPlanDetails | null>(null);
  const [isLoadingStripe, setIsLoadingStripe] = useState(false)
  const [isLoading, setIsLoading] = useState(true);
  const stripe = new Stripe();

  const getSubscription = useCallback(async () => {
    const subscription = await stripe.getUsersSubscription(user.uid).get();
    if (subscription.empty) {
      const plan = await stripe.getFreePlan();
      console.log({ plan })
      setSubscription(plan);
    }
    subscription.docs.forEach((doc) => {
      const subscription = doc.data() as ISubscription;
      const product = subscription.items[0];
      const currencySymbol = UserHelper.CurrencyToSymbol[product.plan.currency] ?
        UserHelper.CurrencyToSymbol[product.plan.currency] : product.plan.currency
      const plan: IPlanDetails = {
        name: product.price.product.name,
        currency: currencySymbol,
        price: product.plan.amount / 100,
        image: product.price.product.images[0],
        interval: product.plan.interval,
        usage: {}
      }
      setSubscription(plan);
      setHasPaidSubscription(true)
    })
  }, []);


  useEffect(() => {
    getSubscription()
    stripe.getCustomerPortalUrl().then((data) => {
      console.log('Portal URl Retrieved')
      setIsLoading(false)
      if (data.url) {
        setPortalUrl(data.url)
      }
    }).catch(err => {
      console.error(err)
    });
  }, [])

  const updatePlan = async () => {
    if (portalUrl && hasPaidSubscription === true) {
      window.location.assign(portalUrl)
      return;
    }
    setIsLoadingStripe(true)
    if (!process.env.STRIPE_PUBLIC_KEY) {
      console.error('no stripe key')
    }
    const item: ICheckoutSessionItem = {
      price: PRICING_DETAILS.PAID_TIER_PRICE_ID,
      success_url: window.location.origin,
      cancel_url: window.location.origin
    }
    const checkoutSession = await stripe.addCheckoutItem(user.uid, item)
    checkoutSession.onSnapshot(async (snap) => {
      const data = snap.data();
      if (!data) {
        alert('error while checking out')
        console.error({ data })
        return;
      }
      const { error, sessionId } = data;
      if (sessionId) {
        const stripeJS = await loadStripe(process.env.STRIPE_PUBLIC_KEY as string)
        if (stripeJS) {
          setIsLoadingStripe(false)
          stripeJS.redirectToCheckout({ sessionId });
        }
      }
    })
  }

  if (!subscription) {
    return null;
  }

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Manage your subscription" />
      <Divider />
      <Backdrop className={classes.backdrop} open={isLoadingStripe}>
        <CircularProgress />
      </Backdrop>
      <CardContent>
        <Paper variant="outlined">
          <Box className={classes.overview}>
            <div>
              <Typography
                display="inline"
                variant="h4"
                color="textPrimary"
              >

                {`${subscription.currency}${subscription.price}`}
              </Typography>
              <Typography
                display="inline"
                variant="subtitle1"
              >
                /{subscription.interval}
              </Typography>
            </div>
            <Box
              display="flex"
              alignItems="center"
              mb={1}
            >
              <img
                alt="Product"
                className={classes.productImage}
                src={subscription.image}
              />
              <Typography
                variant="overline"
                color="textSecondary"
              >
                {subscription.name}
              </Typography>
            </Box>
          </Box>
          <Divider />
          <Box className={classes.details}>
            <div>
              <Typography
                variant="body2"
                color="textPrimary"
              >
                {`${user.usage.messages}/${subscription?.usage?.messages} Messages left`}
              </Typography>
              <Typography
                variant="body2"
                color="textPrimary"
              >
                {`${user.usage.profileViews}/${subscription?.usage?.profileViews} Profile Views left`}
              </Typography>
            </div>
            {/*<div>
              <Typography
                variant="body2"
                color="textPrimary"
              >
                {`30 invites left`}
              </Typography>
              <Typography
                variant="body2"
                color="textPrimary"
              >
                {`30 ads left`}
              </Typography>
            </div>*/}
          </Box>
        </Paper>
        <Box
          mt={2}
          display="flex"
          justifyContent="flex-end"
        >
          <Button
            size="small"
            color="secondary"
            variant="contained"
            onClick={updatePlan}
            disabled={isLoadingStripe || isLoading}
          >
            {isLoading ? <CircularProgress size="sm" /> : hasPaidSubscription === true ? 'Manage Plan' : 'Upgrade plan'}
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
}

Subscription.propTypes = {
  className: PropTypes.string
};

export default Subscription;
