import {
  Avatar,
  Box,

  Card,

  CardContent,

  makeStyles, Typography
} from '@material-ui/core';
import { IProfile } from '@warnster/shared-interfaces';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  root: {},
  name: {
    marginTop: theme.spacing(1)
  },
  avatar: {
    height: 100,
    width: 100
  }
}));

interface IProfileDetails {
  profile: IProfile,
  className?: string
}

function ProfileDetails({ profile, className, ...rest }: IProfileDetails) {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Box
          display="flex"
          alignItems="center"
          flexDirection="column"
          textAlign="center"
        >
          <Avatar
            className={classes.avatar}
            src={profile.avatar}
          />
          <Typography
            className={classes.name}
            gutterBottom
            variant="h3"
            color="textPrimary"
          >
            {profile.name}
          </Typography>
          <Typography
            color="textPrimary"
            variant="body1"
          >
            New York, USA
          </Typography>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            4:32PM (GMT-4)
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
}

ProfileDetails.propTypes = {
  className: PropTypes.string,
  profile: PropTypes.object.isRequired
};

export default ProfileDetails;
