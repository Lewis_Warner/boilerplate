
import { Grid, makeStyles } from '@material-ui/core';
import { IAppState } from '@warnster/shared-library';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React from 'react';
import { useSelector } from 'react-redux';
import ProfileForm from '../../../components/Profile/ProfileForm';
import ProfileDetails from './ProfileDetails';

const useStyles = makeStyles(() => ({
  root: {}
}));

interface IGeneralProps {
  className?: string
}

function General({ className, ...rest }: IGeneralProps) {
  const classes = useStyles();
  const { user, profile } = useSelector((state: IAppState) => state.account);

  return (
    <Grid
      className={clsx(classes.root, className)}
      container
      spacing={3}
      {...rest}
    >
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <ProfileDetails profile={profile} />
      </Grid>
      <Grid
        item
        lg={8}
        md={6}
        xl={9}
        xs={12}
      >
        <ProfileForm />
      </Grid>
    </Grid>
  );
}

General.propTypes = {
  className: PropTypes.string
};

export default General;
