import { Box, CircularProgress } from "@material-ui/core"
import React from "react"

export const Spinner = ({ type }: { type: "relative" | "absolute" }) => {
    if (type === 'relative') {
        return (
            <Box position="relative" display="flex" marginLeft="50%" left={-20}>
                <CircularProgress />
            </Box>
        )
    }
    if (type === "absolute") {
        <Box position="absolute" top="50%" left="50%">
            <CircularProgress />
        </Box>
    }
    return <CircularProgress />
}