/* globals window */
import {
  IAuthCookie, ISignInUser,

  IUser,

  IUserRetrievedFirebase
} from "@warnster/shared-interfaces";
import {
  AccountActions,

  ProfileDaoClient, UserDaoClient,

  UtilHelper
} from "@warnster/shared-library";
import firebase from "firebase";
import firebaseui from "firebaseui";
import 'firebaseui/dist/firebaseui.css';
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setAuthCookie } from "../store/persistant/cookieStore";


const config = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_PUBLIC_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
  if (process.env.LOCAL_DEV === "development") {
    firebase.firestore().useEmulator("localhost", 8080);
    firebase.auth().useEmulator("http://localhost:9099");
    firebase.functions().useEmulator("localhost", 5001);
  }
}


// Init the Firebase app.
const userDao = new UserDaoClient();

const FirebaseAuth = () => {
  const router = useRouter();
  const profileDao = new ProfileDaoClient();
  const dispatch = useDispatch();
  let ui: firebaseui.auth.AuthUI;

  const firebaseAuthConfig: firebaseui.auth.Config = {
    signInFlow: "popup",
    // Auth providers
    // https://github.com/firebase/firebaseui-web#configure-oauth-providers
    signInOptions: [
      {
        provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
        requireDisplayName: false,
      },
      {
        provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      },
      {
        provider: firebase.auth.TwitterAuthProvider.PROVIDER_ID
      },
      {
        provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID
      }
    ],
    credentialHelper: "none",
    callbacks: {
      // @ts-ignore
      signInSuccessWithAuthResult: ({ user }: { user: ISignInUser }, redirectUrl: string) => {
        // xa is the access token, which can be retrieved through
        const { uid, email, xa } = user;
        const userData: IAuthCookie = {
          uid,
          email,
          token: xa
        };
        console.log('ERROR HERE?')
        profileDao.getProfileByUserID(uid).then((profile) => {
          console.log({ profile })
          if (profile) {
            dispatch(AccountActions.setProfile(profile))
          }
        }).catch((err) => {
          console.error('error ', err)
        })
        userDao.getUser(uid).get().then((doc) => {
          let user = doc.data();
          if (!user) {
            redirectUrl = UtilHelper.STEP_SETUP_URL
          } else {
            //get the profile and store in redux

            //redirect for login
            const u = user as unknown as IUser;
            redirectUrl = UtilHelper.getRedirectUrlForLogin(u);
            console.log({ u, redirectUrl })
            dispatch(AccountActions.login(u as unknown as IUserRetrievedFirebase));
          }
          setAuthCookie(userData);
          router.push(redirectUrl);
          return false;
        }).catch((err) => {
          console.error('error ', err)
        });
        console.log('there')

      },
    },
  };

  // Do not SSR FirebaseUI, because it is not supported.
  // https://github.com/firebase/firebaseui-web/issues/213
  const [renderAuth, setRenderAuth] = useState(false);
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRenderAuth(true);
      //import firebase ui only on the frontend as it tries to bind to .window;
      if (!window.firebaseui) {
        window.firebaseui = require('firebaseui');
      }
      // Initialize the FirebaseUI Widget using Firebase.
      try {
        ui = new window.firebaseui.auth.AuthUI(firebase.auth());
        ui.start('#firebaseui-auth-container', firebaseAuthConfig);

      } catch (err) {
        console.error(err)
      }
      // The start method will wait until the DOM is loaded.
    }
    return () => {
      if (ui) {
        ui.delete()
      }
    }
  }, []);
  return (
    <div>
      <div id="firebaseui-auth-container"></div>
    </div>
  );
};

export default FirebaseAuth;
