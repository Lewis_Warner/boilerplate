import { Avatar, Box, Link, makeStyles, Typography } from "@material-ui/core";
import { IMessage, IMessageGroup } from "@warnster/shared-interfaces";
import clsx from "clsx";
import moment from "moment";
import NextLink from "next/link";
import React, { useState } from "react";
import { Lightbox } from "react-modal-image";
import { useSelector } from "react-redux";
import { IWebAppState } from "../../interfaces/state_interfaces";

type TMessageDisplay = "user" | "not-user" | 'system';

function senderSelector(
  state: IWebAppState,
  message: IMessage,
  group: IMessageGroup
): { avatar: string; name: string; type: TMessageDisplay } {
  const { profile } = state.account;
  const senderId = message.senderId
  if (message.contentType === 'system') {
    return {
      avatar: "",
      name: "",
      type: "system",
    };
  }
  if (senderId !== profile.userID) {
    if (group.members[senderId]) {
      const recip = group.members[senderId];
      return {
        avatar: recip.avatar,
        name: recip.name,
        type: "not-user",
      };
    }
    return {
      avatar: "",
      name: "Deleted User",
      type: "not-user",
    };
  }
  return {
    avatar: profile.avatar,
    name: "Me",
    type: "user",
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
    display: "flex",
  },
  avatar: {
    height: 32,
    width: 32,
  },
  image: {
    cursor: "pointer",
    height: "auto",
    maxWidth: "100%",
    maxHeight: 200
  },
  message: {
    maxWidth: "100%",
    overflowWrap: "anywhere",
  },
  user: {
    marginLeft: "auto",
    "& .MuiBox-root": {
      "& div:first-child": {
        color: theme.palette.secondary.contrastText,
        backgroundColor: theme.palette.secondary.main,
        "& .MuiBox-root": {
          marginTop: 8
        }
      }
    }
  },
  nonUser: {
    marginLeft: 0,
    "& .MuiBox-root": {
      "& div:first-child": {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.background.default,
        "& .MuiBox-root": {
          marginTop: 8
        }
      }
    }
  },
  system: {
    marginRight: "auto",
    marginLeft: "auto",
    "& .MuiBox-root": {
      "& div:first-child": {
        color: theme.palette.warning.contrastText,
        backgroundColor: theme.palette.warning.main,
      }
    }
  }
}));

interface MessageProps {
  className?: string;
  message: IMessage;
  group: IMessageGroup;
}

function Message({ className, message, group, ...rest }: MessageProps) {
  const classes = useStyles();
  const [openedFile, setOpenedFile] = useState<string | null>(null);
  const sender = useSelector((state: IWebAppState) =>
    senderSelector(state, message, group)
  );

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box
        display="flex"
        maxWidth={500}
        className={sender.type === "user" ? classes.user : sender.type === "not-user" ? classes.nonUser : classes.system}
      >
        {message.contentType !== "system" ? <Avatar className={classes.avatar} src={sender.avatar} /> : ''}
        <Box ml={2}>
          <Box
            py={1}
            px={2}
            borderRadius="borderRadius"
            boxShadow={1}
          >
            {message.contentType !== "system" ?
              <NextLink href="/profile/user" passHref>
                <Link color="inherit" variant="h6">
                  {sender.name}
                </Link>
              </NextLink> : ''}

            <Box>
              {message.contentType === "image" ? (
                <Box mt={2} onClick={() => setOpenedFile(message.body)}>
                  <img
                    alt="Attachment"
                    className={classes.image}
                    src={message.body}
                  />
                </Box>
              ) : (
                <Typography
                  className={classes.message}
                  color="inherit"
                  variant="body1"
                >
                  {message.body}
                </Typography>
              )}
            </Box>
          </Box>
          <Box mt={1} display="flex" justifyContent="flex-end">
            <Typography noWrap color="textSecondary" variant="caption">
              {message.createdAt ? moment(message.createdAt).fromNow() : ""}
            </Typography>
          </Box>
        </Box>
      </Box>
      {openedFile && (
        <Lightbox large={openedFile} hideDownload={true} onClose={() => setOpenedFile(null)} />
      )}
    </div>
  );
}

export default Message;
