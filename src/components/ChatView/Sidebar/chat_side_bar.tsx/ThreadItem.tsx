import {
  Avatar,
  Box,
  Chip,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles
} from "@material-ui/core";
import { IMessageGroup } from "@warnster/shared-interfaces";
import { ChatHelper, IAppState } from "@warnster/shared-library";
import clsx from "clsx";
import NextLink from "next/link";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  active: {
    boxShadow: `inset 4px 0px 0px ${theme.palette.secondary.main}`,
    backgroundColor: theme.palette.action.selected,
  },
  avatar: {
    height: 40,
    width: 40,
  },
  unreadIndicator: {
    marginTop: 2,
    padding: 2,
    height: 18,
    minWidth: 18,
  },
}));

interface ThreadItem {
  active: boolean;
  group: IMessageGroup;
  className?: string;
  closeSidebar: () => void;
}

function ThreadItem({
  active,
  group,
  className,
  closeSidebar,
  ...rest
}: ThreadItem) {
  const classes = useStyles();
  const { account, chat } = useSelector((state: IAppState) => state);
  const { user } = account;
  const lastMessage = group.recentMessage;
  const lastMessageInfo = () => {
    let content = ''
    if (lastMessage.senderId === user.uid) {
      content = 'Me: '
    } else if (lastMessage.senderId !== user.uid && lastMessage.contentType !== 'system') {
      content = group.members[lastMessage.senderId].name + ": "
    }
    if (lastMessage.contentType === 'image') {
      content += "Sent a photo"
    } else {
      content += lastMessage.body
    }
    return content;
  }

  const groupDisplayInfo = ChatHelper.getGroupAvatarAndName(group, user.uid);
  return (
    <NextLink href="/chat/[groupID]" as={`/chat/${group.id}`}>
      <ListItem
        onClick={closeSidebar}
        button
        className={clsx(
          {
            [classes.active]: active,
          },
          className
        )}
        {...rest}
      >
        <ListItemAvatar>
          <Avatar
            alt="Person"
            className={classes.avatar}
            src={groupDisplayInfo.avatar}
          />
        </ListItemAvatar>
        <ListItemText
          primary={groupDisplayInfo.name}
          primaryTypographyProps={{
            noWrap: true,
            variant: "h6",
            color: "textPrimary",
          }}
          secondary={lastMessageInfo()}
          secondaryTypographyProps={{
            noWrap: true,
            variant: "body2",
            color: "textSecondary",
          }}
        />
        <Box ml={2} display="flex" flexDirection="column" alignItems="flex-end">
          {group.unread[user.uid] > 0 && (
            <Chip
              className={classes.unreadIndicator}
              color="secondary"
              size="small"
              label={group.unread[user.uid]}
            />
          )}
        </Box>
      </ListItem>
    </NextLink>
  );
}

ThreadItem.propTypes = {
  active: PropTypes.bool,
  className: PropTypes.string,
};

ThreadItem.defaultProps = {
  active: false,
  className: "",
};

export default ThreadItem;
