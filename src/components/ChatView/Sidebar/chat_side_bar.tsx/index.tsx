import {
  Avatar,
  Box,
  ClickAwayListener,
  Input,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  SvgIcon,
  Typography
} from "@material-ui/core";
import { EGroupType, IMessageGroup } from '@warnster/shared-interfaces';
import {
  ChatHelper, IAppState
} from "@warnster/shared-library";
import clsx from "clsx";
import NextLink from "next/link";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { Search as SearchIcon } from "react-feather";
import { useSelector } from "react-redux";
import ThreadItem from "./ThreadItem";

const useStyles = makeStyles((theme) => ({
  root: {},
  searchContainer: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  search: {
    display: "flex",
    alignItems: "center",
    height: 44,
    borderRadius: 22,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
  },
  searchInput: {
    flexGrow: 1,
    marginLeft: theme.spacing(1),
  },
  contactAvatar: {
    height: 32,
    width: 32,
  },
  threadList: {},
  hideThreadList: {
    display: "none",
  },
}));

function formatGroupForSearch(
  groups: IMessageGroup[],
  userID: string
): { id: string; name: string; avatar: string }[] {
  return groups.map((group) => {
    if (group.type !== EGroupType.PrivateChat) {
      return {
        id: group.id,
        name: group.name,
        avatar: group.avatar,
      };
    } else {
      const recipient: any = { name: "", avatar: "" };
      ChatHelper.traverseGroupMembers(group, (uid, userData) => {
        if (userID !== uid) {
          (recipient.name = userData.name),
            (recipient.avatar = userData.avatar);
        }
      });
      return {
        id: group.id,
        name: recipient.name,
        avatar: recipient.avatar,
      };
    }
  });
}

interface ChatSideBarProps {
  className?: string;
  closeSidebar: () => void;
}

function ChatSideBar({ className, closeSidebar, ...rest }: ChatSideBarProps) {
  const classes = useStyles();
  const router = useRouter();
  const { chat, account } = useSelector((state: IAppState) => state);
  const { groups, selectedGroup } = chat;
  const { user } = account;
  const [searchText, setSearchText] = useState("");
  const [displaySearchResults, setDisplaySearchResults] = useState(false);
  const [searchableGroups, setSearchableGroups] = useState<
    { id: string; name: string; avatar: string }[]
  >([]);
  const handleSearchFocus = (event: any) => {
    event.persist();
    setDisplaySearchResults(true);
  };

  const handleSearchChange = (event: any) => {
    event.persist();
    setSearchText(event.target.value);
  };

  const handleSearchClickAway = () => {
    if (displaySearchResults) {
      setSearchText("");
      setDisplaySearchResults(false);
    }
  };

  useEffect(() => {
    if (displaySearchResults) {
      setDisplaySearchResults(false);
    }
    // eslint-disable-next-line
  }, [router.pathname]);

  useEffect(() => {
    if (searchText === "") {
      setSearchableGroups(formatGroupForSearch(groups, user.uid));
      return;
    }
    const newSearchable = searchableGroups.filter((searchGroup) => {
      const regex = new RegExp(`.*${searchText}.*`, "gi");
      return searchGroup.name.match(regex);
    });
    setSearchableGroups(newSearchable);
  }, [searchText, groups]);

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <ClickAwayListener onClickAway={handleSearchClickAway}>
        <div className={classes.searchContainer}>
          <div className={classes.search}>
            <SvgIcon fontSize="small" color="action">
              <SearchIcon />
            </SvgIcon>
            <Input
              className={classes.searchInput}
              disableUnderline
              onChange={handleSearchChange}
              onFocus={handleSearchFocus}
              placeholder="Search contacts"
              value={searchText}
            />
          </div>
          {displaySearchResults && (
            <Box mt={2}>
              <Typography variant="h6" color="textSecondary">
                Chats
              </Typography>
              <List>
                {searchableGroups.map((group, index) => {
                  return (
                    <NextLink
                      key={index}
                      href="/chat/[groupID]"
                      as={`/chat/${group.id}`}
                    >
                      <ListItem button onClick={closeSidebar}>
                        <ListItemAvatar>
                          <Avatar
                            src={group.avatar}
                            className={classes.contactAvatar}
                          />
                        </ListItemAvatar>
                        <ListItemText
                          primary={group.name}
                          primaryTypographyProps={{
                            noWrap: true,
                            variant: "h6",
                            color: "textPrimary",
                          }}
                        />
                      </ListItem>
                    </NextLink>
                  );
                })}
              </List>
            </Box>
          )}
        </div>
      </ClickAwayListener>
      <List
        className={clsx(classes.threadList, {
          [classes.hideThreadList]: displaySearchResults,
        })}
      >
        {groups.map((group) => {
          const selectGroupID = selectedGroup ? selectedGroup.id : null;
          return (
            <ThreadItem
              closeSidebar={closeSidebar}
              key={group.id}
              group={group}
              active={group.id === selectGroupID}
            />
          );
        })}
      </List>
    </div>
  );
}

ChatSideBar.propTypes = {
  className: PropTypes.string,
};

export default ChatSideBar;
