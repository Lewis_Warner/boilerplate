import { Drawer, Hidden, makeStyles } from "@material-ui/core";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IWebAppState } from "../../../interfaces/state_interfaces";
import { closeSidebar } from "../../../store/actions/uiActions";
import ThreadList from "./chat_side_bar.tsx";
import Toolbar from "./Toolbar";

const useStyles = makeStyles(() => ({
  drawerDesktopRoot: {
    width: 280,
    flexShrink: 0,
  },
  drawerDesktopPaper: {
    position: "relative",
  },
  drawerMobilePaper: {
    position: "relative",
    width: 280,
  },
  drawerMobileBackdrop: {
    position: "absolute",
  },
}));

function Sidebar({ containerRef }: { containerRef: any }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useRouter();
  const { chatSidebarOpen } = useSelector((state: IWebAppState) => state.ui);

  const handleCloseSidebar = () => {
    dispatch(closeSidebar());
  };

  useEffect(() => {
    /*if (!sidebarOpen) {
      dispatch(openSidebar());
    }*/
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.pathname]);

  const content = (
    <div>
      <Toolbar />
      <ThreadList closeSidebar={handleCloseSidebar} />
    </div>
  );

  return (
    <>
      <Hidden smDown>
        <Drawer
          variant="permanent"
          classes={{
            root: classes.drawerDesktopRoot,
            paper: classes.drawerDesktopPaper,
          }}
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdUp>
        <Drawer
          variant="temporary"
          open={chatSidebarOpen}
          onClose={handleCloseSidebar}
          classes={{
            paper: classes.drawerMobilePaper,
          }}
          style={{ position: "absolute", zIndex: 1200 }}
          BackdropProps={{ classes: { root: classes.drawerMobileBackdrop } }}
          ModalProps={{ container: () => containerRef.current }}
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
}

Sidebar.propTypes = {
  containerRef: PropTypes.any,
};

export default Sidebar;
