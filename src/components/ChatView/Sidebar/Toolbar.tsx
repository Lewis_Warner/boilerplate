import {
  Box,
  IconButton,
  makeStyles,
  SvgIcon,
  Typography
} from "@material-ui/core";
import clsx from "clsx";
import NextLink from "next/link";
import React from "react";
import { Edit as EditIcon, Settings as SettingsIcon } from "react-feather";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    height: 64,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

interface ToolbarProps {
  className?: string;
}

function Toolbar({ className, ...rest }: ToolbarProps) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Typography variant="h3" color="textPrimary">
        Chats
      </Typography>
      <Box flexGrow={1} />
      <IconButton>
        <SvgIcon fontSize="small">
          <SettingsIcon />
        </SvgIcon>
      </IconButton>
      <NextLink
        href={{
          pathname: "/chat",
          query: {
            sidebarOpen: false,
          },
        }}
      >
        <IconButton>
          <SvgIcon fontSize="small">
            <EditIcon />
          </SvgIcon>
        </IconButton>
      </NextLink>
    </div>
  );
}

export default Toolbar;
