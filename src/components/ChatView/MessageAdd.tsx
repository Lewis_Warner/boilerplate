import {
  Avatar,
  Box,
  Divider,
  IconButton,
  Input,
  makeStyles,
  Paper,
  SvgIcon,
  Tooltip
} from "@material-ui/core";
import AddPhotoIcon from '@material-ui/icons/AddPhotoAlternate';
import { IAppState } from "@warnster/shared-library";
import clsx from "clsx";
import PropTypes from "prop-types";
import React, { useRef, useState } from "react";
import { Send as SendIcon } from "react-feather";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(1, 2),
  },
  divider: {
    width: 1,
    height: 24,
  },
  fileInput: {
    display: "none",
  },
}));

interface MessageAddProps {
  className?: string;
  disabled?: boolean;
  isBlank?: boolean;
  handleSend: (body: string) => boolean;
  handleFileSelected?: (files: File[]) => void
}

function MessageAdd({
  className,
  disabled,
  isBlank,
  handleSend,
  handleFileSelected,
  ...rest
}: MessageAddProps) {
  const classes = useStyles();
  const { profile } = useSelector((state: IAppState) => state.account);
  const fileInputRef = useRef<HTMLInputElement>(null);
  const [body, setBody] = useState("");
  const messageInputRef = useRef<HTMLInputElement>(null);
  const attachments: any[] = [];

  const handleChange = (event: any) => {
    event.persist();
    setBody(event.target.value);
  };

  const send = () => {
    if (!body || disabled) {
      return;
    }
    const sent = handleSend(body);
    if (sent) {
      setBody("");
    }
    (messageInputRef.current?.firstChild as HTMLInputElement).focus();
  };

  const handleKeyUp = (event: any) => {
    if (event.keyCode === 13) {
      send();
    }
  };

  const handleAttach = () => {
    if (fileInputRef.current !== null) {
      fileInputRef.current.click();
    }
  };

  const handleFileInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const files = Array.from(e.target.files)
      handleFileSelected ? handleFileSelected(files) : '';
    }
  }

  if (isBlank === true) {
    return <div className={clsx(classes.root, className)} {...rest}></div>;
  }
  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Avatar alt="Person" src={profile.avatar} />
      <Paper
        variant="outlined"
        component={Box}
        {...{ flexGrow: 1, ml: 2, p: 1 }}
      >
        <Input
          //@ts-ignore
          className={classes.input}
          ref={messageInputRef}
          disableUnderline
          fullWidth
          value={body}
          onChange={handleChange}
          onKeyUp={handleKeyUp}
          placeholder="Leave a message"
        />
      </Paper>
      <Tooltip title="Send">
        <span>
          <IconButton
            color="secondary"
            disabled={!body || disabled}
            onClick={send}
          >
            <SvgIcon fontSize="small">
              <SendIcon />
            </SvgIcon>
          </IconButton>
        </span>
      </Tooltip>
      {handleFileSelected ? (
        <div>
          <Divider className={classes.divider} />
          <Tooltip title="Attach photo">
            <span>
              <IconButton edge="end" onClick={handleAttach} disabled={disabled}>
                <AddPhotoIcon />
              </IconButton>
            </span>
          </Tooltip>
        </div>
      ) : ''}
      {/*
      <Tooltip title="Attach file">
        <span>
          <IconButton edge="end" onClick={handleAttach} disabled={disabled}>
            <AttachFileIcon />
          </IconButton>
        </span>
      </Tooltip>*/}

      <input
        className={classes.fileInput}
        onChange={handleFileInput}
        {...{ ref: fileInputRef }}
        type="file"
        accept="image/*"
      />
    </div>
  );
}

MessageAdd.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

MessageAdd.defaultProps = {
  className: "",
  disabled: false,
};

export default MessageAdd;
