import {
  Avatar,
  Box,
  Hidden,
  IconButton,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Menu,
  MenuItem,
  SvgIcon,
  Tooltip,
  Typography
} from "@material-ui/core";
import { EGroupType } from '@warnster/shared-interfaces';
import { ChatActions, ChatDaoClient, IAppState, UserDaoClient } from "@warnster/shared-library";
import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import React, { useRef, useState } from "react";
import {

  Menu as MenuIcon,
  MoreVertical as MoreIcon,

  Trash as TrashIcon,
  UserMinus as UserMinusIcon,
  UserPlus as UserPlusIcon,
  UserX as UserXIcon
} from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IMenuItem } from "../../../interfaces/ui_interfaces";
import { openSidebar } from "../../../store/actions/uiActions";
import OnlineIndicator from "../../OnlineIndicator";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: 64,
    flexShrink: 0,
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  onlineIndicator: {
    marginRight: theme.spacing(1),
  },
  searchInput: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
  },
  iconError: {
    color: theme.palette.error.main
  },
  iconWarn: {
    color: theme.palette.warning.main
  },
  iconSuccess: {
    color: theme.palette.success.main
  }
}));

interface ToolbarProps {
  className?: string;
  toolbarInfo: { avatar: string; name: string; lastOnline?: Date };
  openUserDialog?: (open: boolean) => void
}


function Toolbar({
  openUserDialog,
  toolbarInfo,
  className,
  ...rest
}: ToolbarProps) {
  const classes = useStyles();
  const moreRef = useRef(null);
  const chatDao = new ChatDaoClient();
  const userDao = new UserDaoClient();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { selectedGroup } = useSelector((state: IAppState) => state.chat);
  const router = useRouter();
  const { user } = useSelector((state: IAppState) => state.account)
  const [openMenu, setOpenMenu] = useState(false);


  const menuItemsPrivate: IMenuItem[] = [
    {
      icon: <MenuIcon />,
      text: "View Profile",
      onClick: () => { }
    }
  ]
  const menuItemsGroupAdmin: IMenuItem[] = [
    {
      iconClass: "iconSuccess",
      icon: <UserPlusIcon />,
      text: "Add User to Group",
      onClick: () => { openUserDialog ? openUserDialog(true) : '' }
    },
    {
      iconClass: "iconWarn",
      icon: <UserMinusIcon fontVariant="primary" />,
      text: "Remove User from Group",
      onClick: () => { console.log('not implemented') }
    },
    {
      iconClass: "iconError",
      icon: <TrashIcon />,
      text: "Delete Group",
      onClick: () => { if (selectedGroup) chatDao.deleteGroup(selectedGroup.id) }
    },
  ]
  const menuItemsGroupMember: IMenuItem[] = [
    {
      icon: <UserXIcon />,
      text: "Leave Group",
      onClick: () => {
        if (selectedGroup) {

          userDao.removeUserFromGroup(selectedGroup, user.uid)
          dispatch(ChatActions.setSelectedGroup(null))
          enqueueSnackbar("You left the group", {
            variant: "warning",
          });
          router.push("/chat?sidebarOpen=false")
        }
      }
    }
  ]

  const handleOpenSidebar = () => {
    dispatch(openSidebar());
  };

  const handleMenuOpen = () => {
    setOpenMenu(true);
  };

  const handleMenuClose = () => {
    setOpenMenu(false);
  };

  const getMenuItems = () => {
    let menuType: IMenuItem[] = menuItemsPrivate
    if (selectedGroup) {
      if (selectedGroup.type !== EGroupType.PrivateChat) {
        if (selectedGroup.createdBy === user.uid) {
          menuType = menuItemsGroupAdmin
        } else {
          menuType = menuItemsGroupMember
        }
      }
    }

    return menuType.map((menu, index) => {
      return (
        <MenuItem key={index} onClick={menu.onClick}>
          <ListItemIcon className={menu.iconClass ? classes[menu.iconClass] : ''}>
            <SvgIcon fontSize="small">
              {menu.icon}
            </SvgIcon>
          </ListItemIcon>
          <ListItemText primary={menu.text} />
        </MenuItem>
      )
    })
  }

  //todo add presence logic
  const isActive = false;

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Hidden mdUp>
        <IconButton className={classes.menuButton} onClick={handleOpenSidebar}>
          <SvgIcon fontSize="small">
            <MenuIcon />
          </SvgIcon>
        </IconButton>
      </Hidden>
      {toolbarInfo && (
        <Box display="flex" alignItems="center">
          <Avatar src={toolbarInfo.avatar} />
          <Box ml={1}>
            <Typography variant="h5" color="textPrimary">
              {toolbarInfo.name}
            </Typography>

            <Box display="flex" alignItems="center">
              {isActive ? (
                <div>
                  <OnlineIndicator
                    className={classes.onlineIndicator}
                    status="online"
                    size="small"
                  />
                  <Typography color="textSecondary" variant="caption">
                    {toolbarInfo.lastOnline ? "Active Now" : ""}
                  </Typography>
                </div>
              ) : (
                <Typography color="textSecondary" variant="caption">
                  {toolbarInfo.lastOnline ? "Active " + moment().fromNow() : ""}
                </Typography>
              )}
              {selectedGroup &&
                selectedGroup.type !== EGroupType.PrivateChat ? (
                <Typography color="textSecondary" variant="caption">
                  You and {Object.keys(selectedGroup.members).length - 1} Others
                </Typography>
              ) : (
                ""
              )}
            </Box>
          </Box>
        </Box>
      )}
      <Box flexGrow={1} />

      <Tooltip title="More options">
        <IconButton onClick={handleMenuOpen} ref={moreRef}>
          <SvgIcon fontSize="small">
            <MoreIcon />
          </SvgIcon>
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={moreRef.current}
        keepMounted
        elevation={1}
        onClose={handleMenuClose}
        open={openMenu}
      >
        {getMenuItems()}
      </Menu>
    </div>
  );
}

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
