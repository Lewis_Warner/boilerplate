import { Box, CircularProgress, makeStyles } from "@material-ui/core";
import { EGroupType, IMessageGroup, IProfile } from '@warnster/shared-interfaces';
import {
  ChatActions, ChatHelper, IAppState,
  ProfileDaoClient
} from "@warnster/shared-library";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Page from "../Page";
import ChatBox from "./chat_box/chat_box";
import ChatBoxNew from "./chat_box/chat_box_new";
import Sidebar from "./Sidebar";

const useStyles = makeStyles((theme) => ({
  root: {
    //@ts-ignore
    backgroundColor: theme.palette.background.dark,
    height: "100%",
    display: "flex",
    overflow: "hidden",
    position: "relative",
  },
}));

//Gets the group if it is local. If not then is undefined and assumed a new chat.
function getGroupFromRouterQuery(
  groupID: string,
  groups: IMessageGroup[],
  userID: string
) {
  let findFunction: (group: IMessageGroup) => boolean;
  if (groupID.length === 20) {
    findFunction = (group) => {
      return group.id === groupID;
    };
  } else {
    findFunction = (group) => {
      let userFound = false;
      let recipientFound = false;
      if (group.type === EGroupType.PrivateChat) {
        ChatHelper.traverseGroupMembers(group, (uid, userData) => {
          if (uid === userID) {
            userFound = true;
          }
        });
        if (userFound === false) {
          return userFound;
        }
        ChatHelper.traverseGroupMembers(group, (uid, userData) => {
          if (uid === groupID) {
            recipientFound = true;
          }
        });
      }
      return recipientFound;
    };
  }
  return groups.find(findFunction);
}

function ChatView({ hasQueryParam = true }: { hasQueryParam?: boolean }) {
  const classes = useStyles();
  const pageRef = useRef(null);
  const { user } = useSelector((state: IAppState) => state.account);
  const { groups, selectedGroup, groupsLoaded } = useSelector(
    (state: IAppState) => state.chat
  );
  const dispatch = useDispatch();
  const [isNewChat, setIsNewChat] = useState(false);
  const [recipientUser, setRecipientUser] = useState<IProfile>();
  const router = useRouter();
  const groupID = router.query.groupID;
  const [pageReady, setPageReady] = useState(false);

  useEffect(() => {
    if (!groupID) {
      if (!hasQueryParam) {
        setPageReady(true);
      }
      return;
    }
    if (groupsLoaded === false) {
      return
    }
    setIsNewChat(false);
    const gID = groupID as string;
    //Creae find function for group locally
    const group = getGroupFromRouterQuery(gID, groups, user.uid);
    if (group) {
      dispatch(ChatActions.setSelectedGroup(group));
      setPageReady(true);
    } else {
      dispatch(ChatActions.setSelectedGroup(null));
      //If not a group then either is a user or group is not local
      const profileDao = new ProfileDaoClient();
      profileDao.getProfileByUserID(gID).then((recipientProfile) => {
        if (recipientProfile) {
          setIsNewChat(true);
          setRecipientUser(recipientProfile);
          setPageReady(true);
        } else {
          console.log({ groupID, group, recipientProfile });
          throw Error("Not a user or group is not local");
        }
      })
    }
  }, [groupID, groupsLoaded]);

  useEffect(() => {
    if (selectedGroup === null) {
      setIsNewChat(true);
    } else {
      setIsNewChat(false);
    }
  }, [selectedGroup]);

  useEffect(() => {
    dispatch(ChatActions.setSelectedGroup(null));
  }, [])

  const renderChatBox = () => {
    if (!pageReady) {
      return (
        <Box position="relative" display="flex" marginLeft="50%" left={-20}>
          <CircularProgress />
        </Box>
      );
    }
    if (selectedGroup) {
      return <ChatBox group={selectedGroup} />;
    } else {
      return <ChatBoxNew recipient={recipientUser} />;
    }
  };

  return (
    <Page className={classes.root} title="Chat" ref={pageRef}>
      <Sidebar containerRef={pageRef} />
      {renderChatBox()}
    </Page>
  );
}

export default ChatView;
