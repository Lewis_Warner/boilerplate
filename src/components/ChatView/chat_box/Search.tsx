import {
  Avatar,
  Box,
  ClickAwayListener,
  Input,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  Paper,
  Popper,
  Typography
} from "@material-ui/core";
import { IProfile } from '@warnster/shared-interfaces';
import { IAppState } from "@warnster/shared-library";
import clsx from "clsx";
import PropTypes from "prop-types";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";

function filterContacts(
  searchableUsers: IProfile[],
  searchText: string
) {
  return searchableUsers.filter((groupChatUser) => {
    const regex = new RegExp(`.*${searchText}.*`, "gi");
    return groupChatUser.name.match(regex);
  });
}

const useStyles = makeStyles((theme) => ({
  root: {},
  input: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    height: 32,
    borderRadius: 16,
  },
  inputDark: {
    //@ts-ignore
    backgroundColor: theme.palette.background.dark,
  },
  inputDefault: {
    backgroundColor: theme.palette.background.default,
  },
  popperOnTop: {
    zIndex: 1301
  }
}));

interface SearchProps {
  className?: string;
  updateSelectedUsers: (user: IProfile) => void;
  users: IProfile[];
  selectedUsers: IProfile[];
  inputTheme: "dark" | "default"
}

function Search({
  inputTheme,
  className,
  updateSelectedUsers,
  users,
  selectedUsers,
  ...rest
}: SearchProps) {
  const classes: any = useStyles();
  const { user } = useSelector((state: IAppState) => state.account);
  const inputRef = useRef(null);
  const [searchText, setSearchText] = useState("");
  const [displaySearchResults, setDisplaySearchResults] = useState(false);
  const [searchableUsers, setSearchableUsers] = useState<IProfile[]>([]);

  const handleSearchChange = (event: any) => {
    event.persist();
    setSearchText(event.target.value);
  };

  const handleResultsClickAway = () => {
    setDisplaySearchResults(false);
  };


  const setBaseSearchableUsers = () => {
    // loop through searchable users and remove ones that are selected
    const newSearchableUsers = users.filter((searchUser) => {
      const isFound = selectedUsers.find((selectedUser) => {
        return (searchUser.userID === selectedUser.userID) || searchUser.userID === user.uid
      })
      if (!isFound) {
        return searchUser
      }
    })
    setSearchableUsers(newSearchableUsers)
  }

  useEffect(() => {
    setBaseSearchableUsers()
  }, [selectedUsers, users])

  useEffect(() => {
    if (searchText === "") {
      setDisplaySearchResults(false)
      setBaseSearchableUsers();
      return;
    }
    if (searchText && !displaySearchResults) {
      setDisplaySearchResults(true);
    }
    const newSearchableUsers = filterContacts(searchableUsers, searchText)
    setSearchableUsers(newSearchableUsers);
  }, [searchText]);


  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Input
        className={classes.input + ' ' + (inputTheme === "dark" ? classes.inputDark : classes.inputDefault)}
        disableUnderline
        onChange={handleSearchChange}
        placeholder="Search contacts"
        value={searchText}
        ref={inputRef}
      />
      <Popper
        anchorEl={inputRef.current}
        open={displaySearchResults}
        placement="bottom-start"
        className={inputTheme === "dark" ? classes.popperOnTop : ''}
      >
        <ClickAwayListener onClickAway={handleResultsClickAway}>
          <Box component={Paper} width={320} maxWidth="100%" mt={1}>
            {searchableUsers.length === 0 ? (
              <Box px={2} pb={2} pt={2} textAlign="center">
                <Typography variant="h4" color="textPrimary" gutterBottom>
                  Nothing Found
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  We couldn&apos;t find any matches for &quot;
                  {searchText}
                  &quot;. Try checking for typos or using complete words.
                </Typography>
              </Box>
            ) : (
              <div>
                <Box px={2} pt={2}>
                  <Typography variant="h6" color="textSecondary">
                    Contacts
                  </Typography>
                </Box>
                <List>
                  {searchableUsers.map((groupChatUser, index) => {
                    return (
                      <ListItem
                        button
                        onClick={() => {
                          setSearchText("");
                          updateSelectedUsers(groupChatUser);
                        }}
                        key={index}
                      >
                        <ListItemAvatar>
                          <Avatar
                            src={groupChatUser.avatar}
                            className={classes.contactAvatar}
                          />
                        </ListItemAvatar>
                        <ListItemText
                          primary={groupChatUser.name}
                          primaryTypographyProps={{
                            noWrap: true,
                            variant: "h6",
                            color: "textPrimary",
                          }}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              </div>
            )}
          </Box>
        </ClickAwayListener>
      </Popper>
    </div>
  );
}

Search.propTypes = {
  className: PropTypes.string,
};

export default Search;
