import {
  AppBar, Box, Button, CircularProgress, Dialog, Divider,
  IconButton, LinearProgress, makeStyles, Slide, Toolbar as ToolbarMat, Typography
} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { IMessage, IMessageGroup, IProfile, IUseMessages, TMessageContentType } from "@warnster/shared-interfaces";
import {
  ChatDaoClient,
  ChatHelper, IAppState, sendMessage, StorageDaoClient,
  useMessages, usePrevious
} from "@warnster/shared-library";
import React, { useEffect, useRef, useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import { addUsersToGroup } from "../../../api/user";
import Message from "../Message";
import MessageAdd from "../MessageAdd";
import Toolbar from "../ThreadDetails/Toolbar";
import SearchContainer from "./search_container";
const useStyles: any = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
  },
  search: {
    marginLeft: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  appBar: {
    position: 'relative',
  },
}));

function readMessages(group: IMessageGroup, userID: string) {
  const chatDao = new ChatDaoClient();
  if (group.unread[userID] !== 0) {
    chatDao.updateGroupUnread(group.id, userID);
  }
}


const Transition = React.forwardRef(function Transition(props, ref) {
  //@ts-ignore
  return <Slide direction="up" ref={ref} {...props} />;
});

function ChatBox({ group }: { group: IMessageGroup }) {
  const classes = useStyles();
  const messagesRef = useRef(null);
  const { chat, account } = useSelector((state: IAppState) => state);
  const { selectedGroup } = chat;
  const { user } = account;
  const chatDao = new ChatDaoClient();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [uploadPercentage, setUploadPercentage] = useState(0);
  const [isUploadingFile, setIsUploadingFile] = useState(false)
  const [selectedUsers, setSelectedUsers] = useState<IProfile[]>([]);
  const [displayMessages, setDisplayMessages] = useState<IMessage[]>([])
  const [messageFilters, setMessageFilters] = useState<IUseMessages>({
    trigger: false,
    limit: 11,
    filters: {
      groupID: group.id
    },
    order: "desc",
    orderColumn: "createdAt"
  })
  const { messages, isLoading, startAt, isLoadingMore, hasMoreMessages, messageChunk } = useMessages(messageFilters)
  const previousIsLoading = usePrevious(isLoading)
  const previousIsLoadingMore = usePrevious(isLoadingMore)
  const [isRenderingMore, setIsRenderingMore] = useState(false)

  useEffect(() => {
    if (group.id) {
      readMessages(group, user.uid);
    }
  }, [group]);

  useEffect(() => {
    //Normal load, show messages and scroll to bottom
    if (previousIsLoading === true && isLoading === false) {
      console.log('should scroll to bottom')
      setDisplayMessages(messages)
      setTimeout(() => { scrollMessagesToBottom() }, 1);
      return;
    } else
      //adding previous messages and getting height
      if (previousIsLoadingMore === true && isLoadingMore === false) {
        setIsRenderingMore(true)
        setTimeout(() => {
          const preMessageDom = document.getElementById('pre-message-dom-render');
          if (preMessageDom) {
            setDisplayMessages(messages)
            setTimeout(() => { offsetScroll(preMessageDom.clientHeight) }, 1);
          }
          setIsRenderingMore(false)
        }, 1000)

      } else if (isLoading === false && isLoadingMore === false) {
        console.log('CALLING SET MESSAGE')
        //Adding an incoming message
        setDisplayMessages(messages)
        setTimeout(() => { scrollMessagesToBottom() }, 1);
      }
  }, [messages, isLoadingMore, isLoading])

  const getMessageRef = () => {
    //@ts-ignore
    const element: HTMLDivElement = messagesRef.current._container;
    return element;
  };

  const scrollMessagesToBottom = () => {
    if (messagesRef.current) {
      const element = getMessageRef();
      element.scrollTop = element.scrollHeight;
    }
  };

  const offsetScroll = (height: number) => {
    if (messagesRef.current) {
      //@ts-ignore
      const element: HTMLDivElement = messagesRef.current._container;
      element.scrollTop = height;
    }
  };

  const handleSend = (body: string, type: TMessageContentType = 'text') => {
    const message = chatDao.createMessage(body, user.uid, type);
    sendMessage(message, group);
    return true;
  };


  const handleFileSelected = (files: File[]): void => {
    const fileExtension = files[0].name.split('.').pop()
    const fileName = uuid();
    const storagePath = `groups/${group.id}/${fileName}.${fileExtension}`
    const storageDao = new StorageDaoClient();
    const storage = storageDao.getFileReference(storagePath);
    setIsUploadingFile(true)
    let uploadTask = storage.put(files[0]);
    uploadTask.on('state_changed',
      (snapshot) => {
        //progress function
        setUploadPercentage((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
      },
      (error) => {
        //errors from file upload
        console.error({ error })
      },
      () => {
        //completed, send image message
        setIsUploadingFile(false)
        handleSend(storagePath, 'image');
      }
    )
  }


  const handleScroll = (e: any) => {
    const element = e.target;
    if (element.scrollTop === 0) {
      //fetch messages
      if (hasMoreMessages === true) {
        setMessageFilters({ ...messageFilters, startAt, trigger: !messageFilters.trigger })
      }
    }
  };

  const getGroupUsers = () => {
    const groupUserIDs: string[] = [];
    if (selectedGroup) {
      ChatHelper.traverseGroupMembers(selectedGroup, (userID, userData) => {
        if (userData.isRemoved === false && userData.isDeleted === false) {
          groupUserIDs.push(userID)
        }
      })
    }
    return groupUserIDs
  }

  const handleClose = () => {
    setDialogOpen(false)
  }

  const handleAddUsers = async () => {
    if (selectedGroup) {
      const result = await addUsersToGroup(selectedGroup, selectedUsers)
    }
    //addUsersToGroup();
    setDialogOpen(false)
  }


  const getFullScreenDialog = () => {
    return (
      //@ts-ignore
      <Dialog fullScreen open={dialogOpen} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <ToolbarMat>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h5" className={classes.title}>
              Contacts
            </Typography>
            <Button autoFocus color="inherit" variant="outlined" onClick={handleAddUsers}>
              Add Users
            </Button>
          </ToolbarMat>
        </AppBar>
        <Box display="flex" alignItems="center" flexWrap="wrap" p={2}>
          <Typography variant="body1" color="textSecondary">
            Select Contacts:
        </Typography>
          <SearchContainer inputTheme="dark" hiddenUsers={getGroupUsers()} updateSelectedUsers={(users) => {
            setSelectedUsers(users);
          }} />

        </Box>
      </Dialog>
    )
  }

  return (
    <div className={classes.root}>
      <div id="pre-message-dom-render" style={{ position: 'absolute', zIndex: -1 }}>
        {messageChunk ? (
          messageChunk.map((message, index: number) => {
            return <Message key={index} message={message} group={group} />;
          })
        ) : ''}
      </div>
      <Toolbar openUserDialog={(open) => { setDialogOpen(open) }} toolbarInfo={ChatHelper.getGroupAvatarAndName(group, user.uid)} />
      <Divider />
      <Box
        flexGrow={1}
        p={2}
        onScroll={handleScroll}
        {...{ ref: messagesRef, options: { suppressScrollX: true } }}
        component={PerfectScrollbar}
      >
        <Box paddingTop="40px"></Box>
        {isLoadingMore || isRenderingMore ? (
          <Box position="relative" display="flex" marginLeft="50%" left={-20}>
            <CircularProgress />
          </Box>
        ) : (
          ""
        )}

        {displayMessages ? (
          displayMessages.map((message, index: number) => {
            return <Message key={index} message={message} group={group} />;
          })
        ) : (
          <Box position="relative" display="flex" marginLeft="50%" left={-20}>
            <CircularProgress />
          </Box>
        )}
        {isUploadingFile ? <LinearProgress variant="determinate" value={uploadPercentage} /> : ''}
      </Box>
      <Divider />
      <MessageAdd handleSend={handleSend} handleFileSelected={handleFileSelected} />
      {getFullScreenDialog()}
    </div>
  );
}

export default ChatBox;
