

import { Box, Chip, makeStyles } from "@material-ui/core";
import { IProfile } from '@warnster/shared-interfaces';
import { IAppState, ProfileDaoClient, useIsMountedRef, useProfiles } from "@warnster/shared-library";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Search from "./Search";
const useStyles: any = makeStyles((theme) => ({
    search: {
        marginLeft: theme.spacing(2),
    },
}));

interface ISearchContainerProps {
    recipient?: IProfile,
    inputTheme: "dark" | "default",
    updateSelectedUsers: (user: IProfile[]) => void,
    hiddenUsers: string[]
}

function SearchContainer({ recipient, updateSelectedUsers, hiddenUsers, inputTheme }: ISearchContainerProps) {

    const [selectedUsers, setSelectedUsers] = useState<IProfile[]>([]);
    const { profile } = useSelector((state: IAppState) => state.account)
    const classes = useStyles();
    const isMountedRef = useIsMountedRef();
    const profileDao = new ProfileDaoClient();
    const { profiles, isLoading } = useProfiles({ filters: { excludeUser: profile.userID }, limit: 20, trigger: false });

    const removeSelectedUser = (user: IProfile) => {
        const newUsersArray: IProfile[] = [];
        selectedUsers.forEach((u) => {
            if (user.userID !== u.userID) {
                newUsersArray.push(u);
            }
        });
        setSelectedUsers(newUsersArray);
    };

    useEffect(() => {
        if (recipient) {
            setSelectedUsers([
                ...selectedUsers,
                recipient,
            ]);
        }
    }, [recipient]);


    useEffect(() => {
        updateSelectedUsers(selectedUsers);
    }, [selectedUsers])

    return (
        <React.Fragment>
            {selectedUsers.map((user, index) => {
                return (
                    <Box key={index} ml={2} mb={0.5} mt={0.5}>
                        <Chip
                            size="small"
                            label={user.name}
                            onDelete={() => {
                                removeSelectedUser(user);
                            }}
                            color="primary"
                        />
                    </Box>
                );
            })}
            <Search
                inputTheme={inputTheme}
                users={profiles}
                selectedUsers={selectedUsers}
                updateSelectedUsers={(user) => {
                    setSelectedUsers([...selectedUsers, user]);
                }}
                className={classes.search}
            />
        </React.Fragment>
    )
}

export default SearchContainer;