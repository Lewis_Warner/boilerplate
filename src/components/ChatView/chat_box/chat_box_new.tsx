import {
  Box,
  Button,

  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  makeStyles,
  TextField,
  Typography
} from "@material-ui/core";
import { EGroupType, IProfile } from '@warnster/shared-interfaces';
import { ChatActions, ChatDaoClient, ChatHelper, IAppState, sendMessage } from "@warnster/shared-library";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeSidebar, openSidebar } from "../../../store/actions/uiActions";
import MessageAdd from "../MessageAdd";
import Toolbar from "../ThreadDetails/Toolbar";
import SearchContainer from "./search_container";


const useStyles: any = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
  },
  search: {
    marginLeft: theme.spacing(2),
  },
}));

function ChatBoxNew({ recipient }: { recipient?: IProfile }) {
  const classes = useStyles();
  const { account, chat } = useSelector((state: IAppState) => state);
  const { profile } = account;
  const { groups } = chat
  const [selectedProfiles, setSelectedProfiles] = useState<IProfile[]>([]);
  const [open, setOpen] = useState(false);
  const [groupName, setGroupName] = useState("");
  const router = useRouter();
  const [groupNameInput, setGroupNameInput] = useState("");
  const dispatch = useDispatch();

  const handleSend = (body: string) => {
    const chatDao = new ChatDaoClient();
    const chatType = getGroupChatType();
    if (groupName === "" && chatType === EGroupType.InviteOnlyGroupChat) {
      handleClickOpen();
      return false;
    }
    const message = chatDao.createMessage(body, profile.userID, "text");
    //Get private group if it exists
    if (chatType === EGroupType.PrivateChat) {
      const privateGroup = ChatHelper.findPrivateChatByUserID(groups, selectedProfiles[0].userID, profile.userID)
      //If private chat exists then send message to existing chat
      if (privateGroup) {
        sendMessage(message, privateGroup);
        dispatch(ChatActions.setSelectedGroup(privateGroup))
        return true;
      }
    }
    const recipients = selectedProfiles;
    recipients.push(profile);
    const { newGroupData } = chatDao.createGroupObjects(
      recipients,
      message,
      profile.userID,
      getGroupNameDisplay()
    );
    dispatch(ChatActions.createGroup(newGroupData, message));
    setSelectedProfiles([]);
    return true;
  };

  const getGroupChatType = () => {
    if (selectedProfiles.length > 1) {
      return EGroupType.InviteOnlyGroupChat
    }
    if (selectedProfiles.length === 1) {
      return EGroupType.PrivateChat
    }
    return null
  }
  const getGroupNameDisplay = () => {
    switch (selectedProfiles.length) {
      case 0:
        return "New Chat";
      case 1:
        return selectedProfiles[0].name;
      default:
        if (groupName === "") {
          return "New Group Chat";
        } else {
          return groupName;
        }
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleChange = (event: any) => {
    event.persist();
    setGroupNameInput(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (router.query.sidebarOpen && router.query.sidebarOpen === "false") {
      dispatch(closeSidebar());
    } else {
      dispatch(openSidebar());
    }
  }, [router]);

  return (
    <div className={classes.root}>
      <Toolbar
        toolbarInfo={{ avatar: "", name: getGroupNameDisplay() }}
      />
      <Divider />
      <Box display="flex" alignItems="center" flexWrap="wrap" p={2}>
        <Typography variant="body1" color="textSecondary">
          To:
        </Typography>
        <SearchContainer inputTheme="default" hiddenUsers={[profile.userID]} recipient={recipient} updateSelectedUsers={(users) => {
          setSelectedProfiles(users);
        }} />
      </Box>
      <Box flexGrow={1} />
      <Divider />
      <MessageAdd
        disabled={selectedProfiles.length === 0}
        handleSend={handleSend}
      />

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Group Name</DialogTitle>
        <DialogContent>
          <DialogContentText>Please enter a group name</DialogContentText>
          <TextField
            margin="dense"
            id="groupName"
            label="Group Name"
            type="text"
            fullWidth
            value={groupNameInput}
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setGroupName("");
              handleClose();
            }}
            color="primary"
          >
            Cancel
          </Button>
          <Button
            onClick={() => {
              setGroupName(groupNameInput);
              handleClose();
            }}
            color="primary"
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default ChatBoxNew;
