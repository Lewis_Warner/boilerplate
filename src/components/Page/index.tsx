import Head from "next/head";
import React, { forwardRef } from "react";

type PageProps = {
  title: string;
  className: string;
  children: any;
};

const Page = forwardRef(({ title, children, ...rest }: PageProps, ref: any) => {

  return (
    <div id="pagee" ref={ref} {...rest}>
      <Head>
        <title>{title}</title>
      </Head>
      {children}
    </div>
  );
});
Page.displayName = "Page";
export default Page;
