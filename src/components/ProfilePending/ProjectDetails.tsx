/* eslint-disable react/no-array-index-key */
/* eslint-disable no-shadow */
import {
  Box,
  Button,
  makeStyles,
  TextField,
  Typography
} from "@material-ui/core";
import { IProfileUpdateDraft, IUserUpdateDraft } from '@warnster/shared-interfaces';
import { AccountActions, IAppState, ProfileDaoClient, UserDaoClient } from "@warnster/shared-library";
import clsx from "clsx";
import { Formik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
  root: {},
  addTab: {
    marginLeft: theme.spacing(2),
  },
  tag: {
    "& + &": {
      marginLeft: theme.spacing(1),
    },
  },
  datePicker: {
    "& + &": {
      marginLeft: theme.spacing(2),
    },
  },
}));

interface ProfileDetailsProps {
  className?: string;
  onBack?: any;
  onNext: any;
  onComplete: any;
}

function ProfileDetails({
  className,
  onBack,
  onComplete,
  onNext,
  ...rest
}: ProfileDetailsProps) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { user, profile } = useSelector((state: IAppState) => state.account)
  const userDao = new UserDaoClient();
  const profileDao = new ProfileDaoClient()
  return (
    <Formik
      initialValues={{
        name: "",
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string()
          .min(3, "Must be at least 3 characters")
          .max(255)
          .required("Required"),
      })}
      onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        try {
          const userDraft: IUserUpdateDraft = {
            profileComplete: true,
          };
          //will update state from base_wrapper listener
          userDao.updateUser(user.uid, userDraft)
          const profileUpdate: IProfileUpdateDraft = {
            name: values.name,
            isComplete: true,
          }
          dispatch(AccountActions.updateProfile(profileUpdate, profile.profileID))
          setStatus({ success: true });
          setSubmitting(false);
          if (onComplete) {
            onComplete();
          }
        } catch (err) {
          setErrors({ name: err.message });
          setStatus({ success: false });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
      }) => {
        return (
          <form
            onSubmit={handleSubmit}
            className={clsx(classes.root, className)}
            {...rest}
          >
            <Typography variant="h3" color="textPrimary">
              Fill in your profile
            </Typography>
            <Box mt={2}>
              <Typography variant="subtitle1" color="textSecondary">
                Complete your profile to access the full site
              </Typography>
            </Box>
            <Box mt={2}>
              <TextField
                error={Boolean(touched.name && errors.name)}
                fullWidth
                helperText={touched.name && errors.name}
                label="Name"
                name="name"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.name}
                variant="outlined"
              />
            </Box>
            <Box mt={6} display="flex">
              {onBack && (
                <Button onClick={onBack} size="large">
                  Previous
                </Button>
              )}
              <Box flexGrow={1} />
              <Button
                color="secondary"
                disabled={isSubmitting}
                type="submit"
                variant="contained"
                size="large"
              >
                Next
              </Button>
            </Box>
          </form>
        );
      }}
    </Formik>
  );
}

export default ProfileDetails;
