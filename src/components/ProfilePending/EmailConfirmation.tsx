import { Box, Button, Typography } from "@material-ui/core";
import React from "react";

interface EmailConfirmationProps {
  onResendMail: any;
}

const EmailConfirmation = ({ onResendMail }: EmailConfirmationProps) => {
  return (
    <div>
      <Typography variant="h3" color="textPrimary">
        Confirm your email
      </Typography>
      <Box mt={2}>
        <Typography variant="subtitle1" color="textSecondary">
          You must confirm your email before proceeding to creating your
          profile.
        </Typography>
      </Box>
      <Box mt={6} textAlign="center">
        <Button
          color="secondary"
          onClick={() => onResendMail()}
          type="button"
          variant="contained"
          size="large"
        >
          Send Confirmation Email
        </Button>
      </Box>
    </div>
  );
};

export default EmailConfirmation;
