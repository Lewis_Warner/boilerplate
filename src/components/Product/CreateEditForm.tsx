import { Box, Button, CircularProgress, TextField } from '@material-ui/core';
import { IProductDetails, IProductUsage } from '@warnster/shared-interfaces';
import { Stripe } from '@warnster/shared-library';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';

interface ICreateEditForm {
    productDetails: IProductDetails
}

const CreateEditForm = ({ productDetails }: ICreateEditForm) => {

    const [productUsage, setProductUsage] = useState<IProductUsage>()
    const [isEdit, setIsEdit] = useState(false)
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        setIsEdit(productDetails.hasUsagePlan)
        const usage: IProductUsage = {
            messages: productDetails?.messages,
            profileViews: productDetails?.profileViews,
        };
        setProductUsage(usage)
        //convert product details to product usage, set and show on form
    }, [])

    const submitForm = async (productUsage: IProductUsage) => {
        const stripe = new Stripe()
        if (isEdit) {
            await stripe.updateProductUsage(productDetails.productID, productUsage);
        } else {
            await stripe.createProductUsage(productDetails.productID, productUsage);
        }
        enqueueSnackbar("Product Details Updated", {
            variant: "success",
            anchorOrigin: {
                vertical: "top",
                horizontal: "right",
            },
            autoHideDuration: 3000,
            persist: false,
        });

    }

    if (!productUsage) {
        return <div></div>
    }

    return (
        <Formik
            initialValues={productUsage}
            validationSchema={Yup.object().shape({
                messages: Yup.number().required('Required'),
                profileViews: Yup.number().required('Required'),
            })}
            onSubmit={async (values, {
                resetForm,
                setErrors,
                setStatus,
                setSubmitting
            }) => {
                try {
                    // Make API request
                    await submitForm(values)
                    resetForm();
                    setStatus({ success: true });
                    setSubmitting(false);
                } catch (error) {
                    setStatus({ success: false });
                    // setErrors({ submit: error.message });
                    setSubmitting(false);
                }
            }}
        >
            {({
                errors,
                handleBlur,
                handleChange,
                handleSubmit,
                isSubmitting,
                touched,
                values
            }) => (
                isSubmitting ? (
                    <Box
                        display="flex"
                        justifyContent="center"
                        my={5}
                    >
                        <CircularProgress />
                    </Box>
                ) : (
                    <form onSubmit={handleSubmit}>
                        <Box mt={2}>
                            <TextField
                                error={Boolean(touched.messages && errors.messages)}
                                fullWidth
                                helperText={touched.messages && errors.messages}
                                label="Messages"
                                name="messages"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="number"
                                value={values.messages}
                                variant="outlined"
                            />
                        </Box>
                        <Box mt={2}>
                            <TextField
                                error={Boolean(touched.profileViews && errors.profileViews)}
                                fullWidth
                                helperText={touched.profileViews && errors.profileViews}
                                label="Profile Views"
                                name="profileViews"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="number"
                                value={values.profileViews}
                                variant="outlined"
                            />
                        </Box>
                        <Box mt={2}>
                            <Button
                                color="secondary"
                                disabled={isSubmitting}
                                fullWidth
                                size="large"
                                type="submit"
                                variant="contained"
                            >
                                Sign up
</Button>
                        </Box>
                    </form>
                )
            )}
        </Formik>

    )
}

export default CreateEditForm