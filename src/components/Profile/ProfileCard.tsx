import {
  Card,
  CardActionArea,
  CardContent,
  Fab,
  makeStyles,
  Typography
} from '@material-ui/core';
import { IProfile } from '@warnster/shared-interfaces';
import { useRouter } from 'next/router';
import React from 'react';
import { Mail } from 'react-feather';
import { ImageCarousel } from '../Images/ImageCarousel';

const useStyles = makeStyles((theme) => ({
  root: {},
  fab: {
    position: 'absolute',
    top: theme.spacing(1),
    right: theme.spacing(1),
    zIndex: 1000,
  }
}));

interface IProfileCardProps {
  profile: IProfile,
  className?: string
}

function ProfileCard({ profile, className, ...rest }: IProfileCardProps) {
  const router = useRouter();
  const classes = useStyles();

  return (
    <Card className={classes.root} onClick={() => { router.push(`/profile/${profile.profileID}`) }}>
      <CardActionArea>
        <Fab onClick={(e) => { router.push(`/chat/${profile.userID}`); e.stopPropagation() }} className={classes.fab} color="primary" aria-label="add">
          <Mail />
        </Fab>
        <ImageCarousel images={profile.images}></ImageCarousel>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {profile.name}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default ProfileCard;
