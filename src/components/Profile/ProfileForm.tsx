import { Box, Button, Card, CardContent, CardHeader, CircularProgress, Divider, TextField } from '@material-ui/core';
import { IProfileUpdate } from '@warnster/shared-interfaces';
import { IAppState } from '@warnster/shared-library';
import firebase from 'firebase';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useSelector } from 'react-redux';
import * as Yup from 'yup';


const ProfileForm = () => {

    const { profile } = useSelector((state: IAppState) => state.account)
    const { enqueueSnackbar } = useSnackbar();
    const FILE_SIZE = 160 * 1024;
    const SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/png"
    ];



    const submitForm = async (profileUpdate: any) => {
        const profileUpdated: IProfileUpdate = { ...profileUpdate, updatedAt: firebase.firestore.FieldValue.serverTimestamp() }
        enqueueSnackbar("Product Details Updated", {
            variant: "success",
            anchorOrigin: {
                vertical: "top",
                horizontal: "right",
            },
            autoHideDuration: 3000,
            persist: false,
        });

    }


    return (
        <div>
            <Formik
                initialValues={{
                    name: profile.name,
                    avatar: profile.avatar,
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required('Required').max(50),
                    avatar: Yup
                        .mixed()
                        .test(
                            "fileSize",
                            "File too large",
                            value => value && value.size <= FILE_SIZE
                        )
                        .test(
                            "fileFormat",
                            "Unsupported Format",
                            value => value && SUPPORTED_FORMATS.includes(value.type)
                        ),
                    images: Yup
                        .mixed()
                        .test(
                            "fileSize",
                            "File too large",
                            value => value && value.size <= FILE_SIZE
                        )
                        .test(
                            "fileFormat",
                            "Unsupported Format",
                            value => value && SUPPORTED_FORMATS.includes(value.type)
                        ),
                })}
                onSubmit={async (values, {
                    resetForm,
                    setErrors,
                    setStatus,
                    setSubmitting
                }) => {
                    try {
                        // Make API request
                        await submitForm(values)
                        resetForm();
                        setStatus({ success: true });
                        setSubmitting(false);
                    } catch (error) {
                        setStatus({ success: false });
                        // setErrors({ submit: error.message });
                        setSubmitting(false);
                    }
                }}
            >
                {({
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    isSubmitting,
                    touched,
                    values
                }) => (
                    isSubmitting ? (
                        <Box
                            display="flex"
                            justifyContent="center"
                            my={5}
                        >
                            <CircularProgress />
                        </Box>
                    ) : (
                        <form onSubmit={handleSubmit}>
                            <Card
                            >
                                <CardHeader title="Profile" />
                                <Divider />
                                <CardContent>
                                    <Box mt={2}>
                                        <TextField
                                            error={Boolean(touched.name && errors.name)}
                                            fullWidth
                                            helperText={touched.name && errors.name}
                                            label="Name"
                                            name="name"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            type="text"
                                            value={values.name}
                                            variant="outlined"
                                        />
                                    </Box>
                                    <Box mt={2}>
                                    </Box>
                                </CardContent>

                                <Divider />
                                <Box
                                    p={2}
                                    display="flex"
                                    justifyContent="flex-end"
                                >
                                    <Button
                                        color="secondary"
                                        disabled={isSubmitting}
                                        type="submit"
                                        variant="contained"
                                    >
                                        Save Changes
              </Button>
                                </Box>
                            </Card>
                        </form>
                    )
                )}
            </Formik>

        </div>
    )
}

export default ProfileForm