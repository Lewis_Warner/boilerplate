import {
  Breadcrumbs,
  Grid,
  Link,
  makeStyles,
  Typography
} from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import clsx from 'clsx';
import NextLink from 'next/link';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  root: {},
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

interface IBreadcrumbs {
  name: string,
  url: string
}

interface IHeaderProps {
  breadcrumbs?: IBreadcrumbs[],
  className?: string,
  pageTitle: string
}

function Header({ pageTitle, className, breadcrumbs, ...rest }: IHeaderProps) {
  const classes = useStyles();

  return (
    <Grid
      alignItems="center"
      container
      justify="space-between"
      spacing={3}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid item>
        {breadcrumbs ?
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs.map((b, i) => {
              if (breadcrumbs.length - 1 === i) {
                return (
                  <Typography
                    variant="body1"
                    color="textPrimary"
                  >
                    {b.name}
                  </Typography>
                )
              }
              return (
                <NextLink
                  href={b.url}
                >
                  <Link
                    variant="body1"
                    color="inherit"
                  >
                    {b.name}
                  </Link>
                </NextLink>
              )
            })}
          </Breadcrumbs>
          : ''}<Typography
            variant="h3"
            color="textPrimary"
          >
          {pageTitle}
        </Typography>
      </Grid>
    </Grid>
  );
}

export default Header;
