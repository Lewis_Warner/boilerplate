import { StepConnector, withStyles } from "@material-ui/core";

const CustomStepConnector = withStyles((theme) => ({
    vertical: {
        marginLeft: 19,
        padding: 0,
    },
    line: {
        borderColor: theme.palette.divider,
    },
}))(StepConnector);

export default CustomStepConnector