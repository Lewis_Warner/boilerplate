import { Avatar, makeStyles } from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import { Icon } from "react-feather";

const useCustomStepIconStyles = makeStyles((theme) => ({
    root: {},
    active: {
        backgroundColor: theme.palette.secondary.main,
        boxShadow: theme.shadows[10],
    },
    completed: {
        backgroundColor: theme.palette.secondary.main,
    },
}));

interface CustomStepIconProps {
    active: boolean;
    completed: boolean;
    icon: Icon;
}

function CustomStepIcon({ active, completed, icon }: CustomStepIconProps) {
    const classes = useCustomStepIconStyles();

    const Icon = icon;

    return (
        <Avatar
            className={clsx(classes.root, {
                [classes.active]: active,
                [classes.completed]: completed,
            })}
        >
            <Icon size="20" />
        </Avatar>
    );
}

export default CustomStepIcon