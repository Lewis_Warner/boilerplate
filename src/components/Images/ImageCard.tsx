import { Button, CardActions, CardHeader, Divider, Fab } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import { makeStyles } from '@material-ui/core/styles';
import { IProfileUpdateDraft } from '@warnster/shared-interfaces';
import { AccountActions, IAppState } from '@warnster/shared-library';
import React, { useState } from 'react';
import {

    Trash as TrashIcon
} from "react-feather";
import { useDispatch, useSelector } from 'react-redux';
import theme from '../../theme';
import { ImageCarousel } from './ImageCarousel';

const useStyles = makeStyles({
    deleteButton: {
        backgroundColor: theme.palette.error.main
    },
    fab: {
        position: 'absolute',
        top: theme.spacing(1),
        right: theme.spacing(1),
        zIndex: 1000,
        color: theme.palette.error.contrastText,
        backgroundColor: theme.palette.error.main,
        '&:hover': {
            backgroundColor: theme.palette.error.dark,
        },
    },
});

interface IImageCardProps {
    imageUrl: string,
    imagePath: string
}

export default function ImageCard() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { profile } = useSelector((state: IAppState) => state.account);
    const [imageIndex, setImageIndex] = useState(0)

    const deleteImage = () => {
        for (let i = 0; i < profile.images.length; i++) {
            if (i === imageIndex) {
                dispatch(AccountActions.removeProfileImage(profile.images[i]))
                break;
            }
        }
    }

    const setPhoto = (image: 'avatar' | 'coverPhoto') => {
        const profileUpdate: IProfileUpdateDraft = {
            [image]: profile.images[imageIndex].url
        }
        dispatch(AccountActions.updateProfile(profileUpdate, profile.profileID))
    }

    if (profile.images.length === 0) {
        return <h3>No Images</h3>
    }

    return (
        <Card>
            <CardHeader title="Uploaded Photos" />
            <Divider />
            <CardActionArea>
                <Fab onClick={() => { deleteImage() }} className={classes.fab} color="primary" aria-label="add">
                    <TrashIcon />
                </Fab>
                <ImageCarousel images={profile.images} onChange={(index) => { setImageIndex(index) }}></ImageCarousel>
            </CardActionArea>

            <CardActions>
                <Button variant="contained" color="primary" onClick={() => { setPhoto('avatar') }}>
                    Set as Avatar
                </Button>
                <Button variant="contained" color="secondary" onClick={() => { setPhoto('coverPhoto') }}>
                    Set as Cover Photo
                </Button>
            </CardActions>
        </Card>
    );
}