import { CardMedia, makeStyles, Theme } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import { IImage } from '@warnster/shared-interfaces';
import React from 'react';
import Carousel from 'react-material-ui-carousel';
const useStyles = makeStyles((theme: Theme) => ({
    imageContainer: {
        height: 240
    },
    containerMargin: {
        marginBottom: '26px'
    },
    emptyIcon: {
        width: '100%',
        height: '100%',
        color: theme.palette.background.default,
        backgroundColor:
            //@ts-ignore
            theme.palette.mode === 'light' ? theme.palette.grey[400] : theme.palette.grey[600],
    },
    /*imageCard: {
        width: 'auto',
        margin: 'auto',
        maxHeight: '100%',
        maxWidth: '100%'
    },*/
    imageCard: {
        height: '100%'
    }
}))

interface IImageCarouselProps {
    images: IImage[],
    onChange?: (index: number) => void
}

export const ImageCarousel = ({ images, onChange }: IImageCarouselProps) => {

    const classes = useStyles()

    if (images.length === 0) {
        return (
            <div className={classes.imageContainer + ' ' + classes.containerMargin}>
                <PersonIcon className={classes.emptyIcon}></PersonIcon>
            </div >
        )
    }

    if (images.length === 1) {
        return (
            <div className={classes.imageContainer + ' ' + classes.containerMargin}>
                <CardMedia
                    className={classes.imageCard}
                    component="img"
                    alt="Photo"
                    image={images[0].url}
                />
            </div >
        )
    }

    return (

        <Carousel
            autoPlay={false}
            navButtonsAlwaysVisible={true}
            onChange={onChange ? onChange : () => { }}
            animation="slide"
        >
            {

                images.map((image, i) => {
                    return (
                        <div key={i} className={classes.imageContainer}>
                            <CardMedia

                                className={classes.imageCard}
                                component="img"
                                alt="Photo"
                                image={image.url}
                            />
                        </div>
                    )
                })
            }
        </Carousel>
    )
}