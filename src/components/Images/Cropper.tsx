import { IImage } from '@warnster/shared-interfaces';
import { AccountActions, IAppState } from "@warnster/shared-library";
import React, { useCallback, useEffect, useRef, useState } from 'react';
import ReactCrop, { Crop } from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { useDispatch, useSelector } from 'react-redux';



interface ICrop extends Crop {
    aspect: number;
    x: number;
    y: number;
    width: number;
    height: number;
    unit: 'px' | '%';
}

export default function ImageCropper({ profileImage }: { profileImage: IImage }) {
    const imgRef = useRef(null);
    const dispatch = useDispatch();
    const { profile } = useSelector((state: IAppState) => state.account)
    const previewCanvasRef = useRef(null);
    const [crop, setCrop] = useState<ICrop>({ unit: '%', width: 30, height: 30, aspect: 1 / 1, x: 0, y: 30 });
    const [completedCrop, setCompletedCrop] = useState<ICrop>({ unit: '%', width: 30, height: 30, aspect: 1 / 1, x: 0, y: 30 });


    const generateDownload = (canvas: HTMLCanvasElement, crop: ICrop) => {
        if (!crop || !canvas) {
            return;
        }

        canvas.toBlob(
            (blob) => {
                if (blob) {
                    const previewUrl = window.URL.createObjectURL(blob);
                    dispatch(AccountActions.setProfileAvatar(blob, 'png'))
                }

            },
            'image/png',
            1
        );
    }

    const onLoad = useCallback((img) => {
        imgRef.current = img;
    }, []);

    useEffect(() => {
        if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
            return;
        }

        const image = imgRef.current as unknown as HTMLImageElement;
        const canvas = previewCanvasRef.current as unknown as HTMLCanvasElement;

        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        const pixelRatio = window.devicePixelRatio;

        canvas.width = completedCrop.width * pixelRatio;
        canvas.height = completedCrop.height * pixelRatio;

        ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
        ctx.imageSmoothingQuality = 'high';

        ctx.drawImage(
            image,
            completedCrop.x * scaleX,
            completedCrop.y * scaleY,
            completedCrop.width * scaleX,
            completedCrop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
    }, [completedCrop]);

    return (
        <div className="App">
            <ReactCrop
                src={profileImage.url}
                onImageLoaded={onLoad}
                crop={crop}
                onChange={(c) => setCrop(c as ICrop)}
                onComplete={(c) => setCompletedCrop(c as ICrop)}
            />
            <div>
                <canvas
                    ref={previewCanvasRef}
                    // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
                    style={{
                        width: Math.round(completedCrop?.width ?? 0),
                        height: Math.round(completedCrop?.height ?? 0)
                    }}
                />
            </div>
            <p>
                Note that the download below won't work in this sandbox due to the
                iframe missing 'allow-downloads'. It's just for your reference.
      </p>
            <button
                type="button"
                disabled={!completedCrop?.width || !completedCrop?.height}
                onClick={() =>
                    generateDownload(previewCanvasRef.current as unknown as HTMLCanvasElement, completedCrop)
                }
            >
                Download cropped image
      </button>
        </div>
    );
}
