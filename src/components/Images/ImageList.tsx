import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { IImage } from '@warnster/shared-interfaces';
import React from 'react';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            overflow: 'hidden',
            backgroundColor: theme.palette.background.paper,
        },
        gridList: {
            // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
            width: "100%",
            transform: 'translateZ(0)',
        },
        titleBar: {
            background:
                'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
                'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
        },
        icon: {
            color: 'white',
        },
    }),
);

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *     featured: true,
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */
export default function ImageList({ images }: { images: IImage[] }) {
    const classes = useStyles();


    return (
        <div className={classes.root}>
            <GridList cellHeight={200} spacing={0} className={classes.gridList}>
                {images.map((image, index) => {
                    let num: number;

                    return (
                        <GridListTile key={index} cols={1} rows={1}>
                            <img src={image.url} alt="photo" />
                        </GridListTile>
                    )
                })}
            </GridList>
        </div>
    );
}