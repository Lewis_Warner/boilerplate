import { GridCellParams } from "@material-ui/data-grid";
import React from 'react';
import Label from "../Label";
import { GridCellExpand } from "./GridCellExpand";
export const YesNoFormatter = ((params: GridCellParams) => {
    const usage: boolean = params.value as boolean;
    if (usage) {
        return (<Label color="success">Yes</Label>)
    } else {
        return (
            <Label color="warning" >
                No
            </Label>
        )
    }
})

export const renderCellExpand = (params: GridCellParams) => {
    return (
        <GridCellExpand
            value={params.value ? params.value.toString() : ''}
            width={params.colDef.width}
        />
    );
}