import React, { ReactElement } from "react";

function Logo(): ReactElement {
  return <img alt="Logo" src="/static/logo.svg" />;
}

export default React.forwardRef(Logo);
