import {
    Box,
    Card,
    CardContent,
    CardMedia,
    Divider,
    Fab,
    IconButton,
    LinearProgress,
    makeStyles,
    TextField,
    Tooltip
} from '@material-ui/core';
import AddPhotoIcon from '@material-ui/icons/AddPhotoAlternate';
import SendIcon from '@material-ui/icons/Send';
import { IPostBase } from '@warnster/shared-interfaces';
import { PostClientDao, StorageDaoClient, UtilHelper } from '@warnster/shared-library';
import clsx from 'clsx';
import { Formik } from 'formik';
import { useSnackbar } from "notistack";
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { Trash } from 'react-feather';
import { useSelector } from 'react-redux';
import * as Yup from 'yup';
import { IWebAppState } from '../../interfaces/state_interfaces';

const useStyles = makeStyles((theme) => ({
    root: {},
    divider: {
        width: 1,
        height: 24
    },
    fileInput: {
        display: 'none'
    },
    imageUpload: {
        height: 'auto',
        borderRadius: '25px'
    },
    imageContainer: {
        margin: theme.spacing(2),
        position: "relative"
    },
    fab: {
        position: 'absolute',
        top: theme.spacing(1),
        right: theme.spacing(1),
        zIndex: 1000,
        color: theme.palette.error.contrastText,
        backgroundColor: theme.palette.error.main,
        '&:hover': {
            backgroundColor: theme.palette.error.dark,
        }
    }
}));

interface IPostAddProps {
    className?: string
}

function PostAdd({ className, ...rest }: IPostAddProps) {
    const classes = useStyles();
    const fileInputRef = useRef<HTMLInputElement>(null);
    const [value, setValue] = useState('');
    const { account } = useSelector((state: IWebAppState) => state);
    const { profile } = account
    const { enqueueSnackbar } = useSnackbar();
    const [uploadedFileBase64, setUploadedFileBase64] = useState<string>("")
    const [uploadFile, setUploadFile] = useState<File | null>(null)
    const [isPosting, setIsPosting] = useState(false)
    const [uploadPercentage, setUploadPercentage] = useState(0)


    const createPost = async (message: string) => {
        const newPost: IPostBase = {
            message,
            liked: false,
            likes: 0,
            author: {
                profileID: profile.profileID,
                avatar: profile.avatar,
                name: profile.name
            }
        }
        try {
            //Upload the file for the post
            let path: string, url: string, id: string;
            if (uploadFile && uploadedFileBase64) {
                const storageDao = new StorageDaoClient()
                id = UtilHelper.uniqueUID()
                const task = await storageDao.uploadFile(uploadFile, `posts/${profile.profileID}/${id}`, (uploadTask) => {
                    setUploadPercentage((uploadTask.bytesTransferred / uploadTask.totalBytes) * 100)
                })
                path = task.ref.fullPath
                url = await task.ref.getDownloadURL();
                newPost.media = {
                    url,
                    path
                }
            }
            const postDao = new PostClientDao();
            const ref = await postDao.createPost(newPost)
            enqueueSnackbar("Post Created", {
                variant: "success",
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                },
                autoHideDuration: 3000,
                persist: false,
            });
            setValue("")
        } catch (err) {
            console.error(err)
            enqueueSnackbar("Oh no!, the post wasn't created. Try again later", {
                variant: "error",
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                },
                autoHideDuration: 3000,
                persist: false,
            });
        } finally {
            setUploadFile(null)
            setUploadedFileBase64("")
            setUploadPercentage(0)
        }
    }

    const displayFile = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (!(e.target.files && e.target.files.length > 0)) {
            setUploadedFileBase64("")
            setUploadFile(null)
            return;
        }
        const uploadedFile = e.target.files[0];
        const base64 = await UtilHelper.getBase64(uploadedFile);
        setUploadedFileBase64(base64 as string)
        setUploadFile(uploadedFile)
        console.log({ uploadedFile, base64 })
    }

    const removeFile = () => {
        setUploadedFileBase64("")
        setUploadFile(null)
    }

    const handleAttach = () => {
        if (fileInputRef.current !== null) {
            fileInputRef.current.click();
        }
    };


    return (
        <Card
            className={clsx(classes.root, className)}
            {...rest}
        >
            <CardContent>
                <Formik
                    initialValues={{ message: '' }}
                    validationSchema={Yup.object().shape({
                        message: Yup.string().required('Required').max(280),
                    })}
                    onSubmit={async (values, {
                        resetForm,
                        setErrors,
                        setStatus,
                        setSubmitting,
                    }) => {
                        try {
                            // Make API request
                            await createPost(values.message)
                            resetForm();
                            setStatus({ success: true });
                            setSubmitting(false);
                        } catch (error) {
                            setStatus({ success: false });
                            // setErrors({ submit: error.message });
                            setSubmitting(false);
                        }
                    }}
                >
                    {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        touched,
                        values
                    }) => (
                        <form onSubmit={handleSubmit}>
                            {uploadFile && isSubmitting &&
                                <LinearProgress
                                    variant="determinate"
                                    value={uploadPercentage}
                                />
                            }
                            <Box
                                display="flex"
                                alignItems="center"
                            >
                                <TextField
                                    error={Boolean(touched.message && errors.message)}
                                    helperText={touched.message && errors.message}
                                    value={values.message}
                                    fullWidth
                                    name="message"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    multiline={true}
                                    placeholder={`What's on your mind, ${account.profile.name}`}
                                    variant="outlined"
                                />
                                <Tooltip title="Send">
                                    <IconButton
                                        type="submit" color={value.length > 0 ? 'primary' : 'default'} disabled={isSubmitting}>
                                        <SendIcon />
                                    </IconButton>
                                </Tooltip>
                                <Divider className={classes.divider} />
                                <Tooltip title="Attach image">
                                    <IconButton
                                        edge="end"
                                        onClick={handleAttach}
                                    >
                                        <AddPhotoIcon />
                                    </IconButton>
                                </Tooltip>
                                <input
                                    className={classes.fileInput}
                                    ref={fileInputRef}
                                    onChange={(e) => displayFile(e)}
                                    type="file"
                                    accept=".jpg,.jpeg,.png"
                                />
                            </Box>

                        </form>
                    )}
                </Formik>
            </CardContent>
            {uploadedFileBase64 ?
                <div className={classes.imageContainer}>

                    <Fab onClick={() => { removeFile() }} className={classes.fab} color="primary" aria-label="add">
                        <Trash />
                    </Fab>
                    <CardMedia
                        component="img"
                        alt="Uploaded Photo"
                        image={uploadedFileBase64}
                        title="Uploaded Photo"
                        className={classes.imageUpload}
                    />
                </div>
                : ''}
        </Card>
    );
}

PostAdd.propTypes = {
    className: PropTypes.string
};

export default PostAdd;
