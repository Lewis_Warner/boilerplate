import {
    Box,



    colors, IconButton,



    makeStyles, Tooltip,
    Typography
} from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ShareIcon from '@material-ui/icons/Share';
import { IPost } from '@warnster/shared-interfaces';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        alignItems: 'center'
    },
    likedButton: {
        color: colors.red[600]
    }
}));

interface IReactionProps {
    post: IPost,
    className?: string
}

function Reactions({ post, className, ...rest }: IReactionProps) {
    const classes = useStyles();
    const [liked, setLiked] = useState(post.liked);
    const [likes, setLikes] = useState<number>(post.likes);

    const handleLike = () => {
        setLiked(true);
        setLikes((prevLikes) => prevLikes + 1);
    };

    const handleUnlike = () => {
        setLiked(false);
        setLikes((prevLikes) => prevLikes - 1);
    };

    return (
        <div
            className={clsx(classes.root, className)}
            {...rest}
        >
            {liked ? (
                <Tooltip title="Unlike">
                    <IconButton
                        className={classes.likedButton}
                        onClick={handleUnlike}
                    >
                        <FavoriteIcon fontSize="small" />
                    </IconButton>
                </Tooltip>
            ) : (
                <Tooltip title="Like">
                    <IconButton onClick={handleLike}>
                        <FavoriteBorderIcon fontSize="small" />
                    </IconButton>
                </Tooltip>
            )}
            <Typography
                color="textSecondary"
                variant="h6"
            >
                {likes}
            </Typography>
            <Box flexGrow={1} />
            <IconButton>
                <ShareIcon fontSize="small" />
            </IconButton>
        </div>
    );
}

Reactions.propTypes = {
    className: PropTypes.string,
    post: PropTypes.object.isRequired
};

export default Reactions;
