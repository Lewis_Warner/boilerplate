import {
    Avatar,
    Box,
    IconButton,
    Link,
    ListItemIcon,
    ListItemText,
    makeStyles, Menu, MenuItem, SvgIcon, Typography
} from '@material-ui/core';
import { IComment } from '@warnster/shared-interfaces';
import { DateHelper, PostClientDao } from '@warnster/shared-library';
import clsx from 'clsx';
import NextLink from 'next/link';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { MoreHorizontal, Trash } from 'react-feather';
import { useSelector } from 'react-redux';
import { IWebAppState } from '../../interfaces/state_interfaces';
import { IMenuItem } from '../../interfaces/ui_interfaces';
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        marginBottom: theme.spacing(2)
    },
    bubble: {
        borderRadius: theme.shape.borderRadius
    },
    iconError: {
        color: theme.palette.error.main
    },
    iconWarn: {
        color: theme.palette.warning.main
    },
    iconSuccess: {
        color: theme.palette.success.main
    },
    profileName: {
        marginRight: theme.spacing(1)
    },
    ownComment: {
        paddingRight: theme.spacing(0),
        paddingTop: theme.spacing(0)
    }
}));

interface ICommentProps {
    comment: IComment,
    className?: string
}

function Comment({ comment, className, ...rest }: ICommentProps) {
    const classes = useStyles();
    const { profile } = useSelector((state: IWebAppState) => state.account)
    const isUserComment = comment.author.profileID === profile.profileID
    const moreRef = useRef(null);
    const [openMenu, setOpenMenu] = useState(false);
    const { enqueueSnackbar } = useSnackbar()
    const handleMenuOpen = () => {
        setOpenMenu(true);
    };

    const handleMenuClose = () => {
        setOpenMenu(false);
    };


    const menuItemsGroupAdmin: IMenuItem[] = [

        {
            iconClass: "iconError",
            icon: <Trash />,
            text: "Delete Comment",
            onClick: async () => {
                const postDao = new PostClientDao();
                try {
                    await postDao.deleteComment(comment.commentID)
                    enqueueSnackbar("Comment Deleted", {
                        variant: "success",
                        anchorOrigin: {
                            vertical: "top",
                            horizontal: "right",
                        },
                        autoHideDuration: 3000,
                        persist: false,
                    });
                } catch (err) {
                    enqueueSnackbar("Error, Try again later", {
                        variant: "error",
                        anchorOrigin: {
                            vertical: "top",
                            horizontal: "right",
                        },
                        autoHideDuration: 3000,
                        persist: false,
                    });
                }
            }
        },
    ]

    const getMenuItems = () => {

        return menuItemsGroupAdmin.map((menu, index) => {
            return (
                <MenuItem key={index} onClick={menu.onClick}>
                    <ListItemIcon className={menu.iconClass ? classes[menu.iconClass] : ''}>
                        <SvgIcon fontSize="small">
                            {menu.icon}
                        </SvgIcon>
                    </ListItemIcon>
                    <ListItemText primary={menu.text} />
                </MenuItem>
            )
        })
    }
    return (
        <div
            className={clsx(classes.root, className)}
            {...rest}
        >
            <NextLink href={`profile/${comment.author.profileID}`}>
                <Avatar
                    alt="Person"
                    src={comment.author.avatar}
                />
            </NextLink>
            <Box
                flexGrow={1}
                p={2}
                ml={2}
                bgcolor="background.dark"
                className={classes.bubble + ' ' + (isUserComment === true ? classes.ownComment : '')}
            >
                <Box
                    display="flex"
                    alignItems="center"
                >
                    <NextLink href={`profile/${comment.author.profileID}`}>
                        <Link
                            color="textPrimary"
                            variant="h6"
                            className={classes.profileName}
                        >
                            {comment.author.name}
                        </Link>
                    </NextLink>
                    <Box flexGrow={1} />
                    {isUserComment ? (
                        <IconButton onClick={handleMenuOpen} ref={moreRef} >
                            <SvgIcon fontSize="small">
                                <MoreHorizontal />
                            </SvgIcon>
                        </IconButton>

                    ) : ''}
                    {isUserComment ? (
                        <Menu
                            anchorEl={moreRef.current}
                            keepMounted
                            elevation={1}
                            onClose={handleMenuClose}
                            open={openMenu}
                        >
                            {getMenuItems()}
                        </Menu>
                    ) : ''
                    }
                </Box>
                <Typography
                    variant="caption"
                    color="textSecondary"
                >
                    {DateHelper.getMoment(comment.createdAt).fromNow()}
                </Typography>
                <Typography
                    variant="body1"
                    color="textPrimary"
                >
                    {comment.message}
                </Typography>
            </Box>
        </div>
    );
}

Comment.propTypes = {
    className: PropTypes.string,
    comment: PropTypes.object.isRequired
};

export default Comment;
