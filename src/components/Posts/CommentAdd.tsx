import {
    Avatar,
    Box,
    IconButton,
    Input,
    makeStyles, Paper,
    Tooltip
} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import { ICommentBase } from '@warnster/shared-interfaces';
import { PostClientDao } from '@warnster/shared-library';
import clsx from 'clsx';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { IWebAppState } from '../../interfaces/state_interfaces';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        alignItems: 'center'
    },
    divider: {
        width: 1,
        height: 24
    },
    fileInput: {
        display: 'none'
    }
}));

interface ICommendAddProps {
    level: number,
    parent: false | string,
    className?: string,
    postID: string
}

function CommentAdd({ level, parent, postID, className, ...rest }: ICommendAddProps) {
    const classes = useStyles();
    const { profile } = useSelector((state: IWebAppState) => state.account);
    const fileInputRef = useRef<HTMLInputElement>(null);
    const [value, setValue] = useState('');
    const [isCommenting, setIsCommenting] = useState(false)
    const { enqueueSnackbar } = useSnackbar();


    const createComment = async () => {
        setIsCommenting(true)
        const newComment: ICommentBase = {
            message: value,
            author: {
                profileID: profile.profileID,
                avatar: profile.avatar,
                name: profile.name
            },
            level,
            parent,
            postID
        }
        try {
            const postDao = new PostClientDao();
            const ref = await postDao.createComment(newComment)
            enqueueSnackbar("Comment Posted", {
                variant: "success",
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                },
                autoHideDuration: 3000,
                persist: false,
            });
            setValue("")
        } catch (err) {
            console.error(err)
            enqueueSnackbar("Oh no!, the comment wasn't created. Try again later", {
                variant: "error",
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                },
                autoHideDuration: 3000,
                persist: false,
            });
        } finally {
            setIsCommenting(false)
        }
    }

    return (
        <div
            className={clsx(classes.root, className)}
            {...rest}
        >
            <Avatar
                alt="Person"
                src={profile.avatar}
            />
            <Paper
                variant="outlined"
                component={Box}
                {...{ flexGrow: 1, ml: 2, py: 0.5, px: 2 }}
            >
                <Input
                    disableUnderline
                    fullWidth
                    value={value}
                    onChange={(event) => {
                        event.persist();
                        setValue(event.target.value);
                    }}
                    placeholder="Leave a message"
                />
            </Paper>
            <Tooltip title="Send">
                <IconButton onClick={() => { createComment() }} color={value.length > 0 ? 'primary' : 'default'}>
                    <SendIcon />
                </IconButton>
            </Tooltip>
            <input
                className={classes.fileInput}
                ref={fileInputRef}
                type="file"
            />
        </div>
    );
}

CommentAdd.propTypes = {
    className: PropTypes.string
};

export default CommentAdd;
