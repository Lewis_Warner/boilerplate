import {
    Avatar,
    Box,
    Card,
    CardActionArea,
    CardHeader,
    CardMedia,
    Divider,
    IconButton,
    Link,

    ListItemIcon,

    ListItemText,

    makeStyles, Menu, MenuItem, SvgIcon, Typography
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import { IPost } from '@warnster/shared-interfaces';
import { DateHelper, PostClientDao, useComments } from '@warnster/shared-library';
import clsx from 'clsx';
import NextLink from 'next/link';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { MoreHorizontal, Trash } from 'react-feather';
import { Lightbox } from 'react-modal-image';
import { useSelector } from 'react-redux';
import { IWebAppState } from '../../interfaces/state_interfaces';
import { IMenuItem } from '../../interfaces/ui_interfaces';
import Comment from './Comment';
import CommentAdd from './CommentAdd';
const useStyles = makeStyles((theme) => ({
    root: { position: "relative" },
    date: {
        marginLeft: 6
    },
    media: {
        height: 500,
        backgroundPosition: 'top'
    },
    title: {
        marginRight: theme.spacing(1)
    },
    deletePostButton: {
        position: "absolute",
        top: theme.spacing(1),
        right: theme.spacing(2)
    },
    iconError: {
        color: theme.palette.error.main
    },
    iconWarn: {
        color: theme.palette.warning.main
    },
    iconSuccess: {
        color: theme.palette.success.main
    }
}));

interface IPostCardProps {
    className?: string,
    post: IPost
}

function PostCard({ className, post, ...rest }: IPostCardProps) {
    const classes = useStyles();
    const moreRef = useRef(null);
    const [openMenu, setOpenMenu] = useState(false);
    const [openedFile, setOpenedFile] = useState<string>();
    const { comments } = useComments({ limit: 20, level: 0, parentID: false, postID: post.postID }, post.postID)
    const { profile } = useSelector((state: IWebAppState) => state.account)
    const [debounce, setDebounce] = useState(false);
    comments.reverse()
    const { enqueueSnackbar } = useSnackbar()

    const isUserPost = profile.profileID === post.author.profileID
    const handleMenuOpen = () => {
        setOpenMenu(true);
    };

    const handleMenuClose = () => {
        setOpenMenu(false);
    };
    const menuItemsGroupAdmin: IMenuItem[] = [

        {
            iconClass: "iconError",
            icon: <Trash />,
            text: "Delete Post",
            onClick: async () => {
                const postDao = new PostClientDao();
                try {
                    await postDao.deletePost(post.postID)
                    enqueueSnackbar("Post Deleted", {
                        variant: "success",
                        anchorOrigin: {
                            vertical: "top",
                            horizontal: "right",
                        },
                        autoHideDuration: 3000,
                        persist: false,
                    });
                } catch (err) {
                    enqueueSnackbar("Error, Try again later", {
                        variant: "error",
                        anchorOrigin: {
                            vertical: "top",
                            horizontal: "right",
                        },
                        autoHideDuration: 3000,
                        persist: false,
                    });
                }
            }
        },
    ]

    const getMenuItems = () => {

        return menuItemsGroupAdmin.map((menu, index) => {
            return (
                <MenuItem key={index} onClick={menu.onClick}>
                    <ListItemIcon className={menu.iconClass ? classes[menu.iconClass] : ''}>
                        <SvgIcon fontSize="small">
                            {menu.icon}
                        </SvgIcon>
                    </ListItemIcon>
                    <ListItemText primary={menu.text} />
                </MenuItem>
            )
        })
    }

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <CardHeader
                    avatar={(
                        <NextLink href={`profile/${post.author.profileID}`}>
                            <Avatar
                                alt="Person"
                                //@ts-ignore
                                className={classes.avatar}
                                src={post.author.avatar}
                            />
                        </NextLink>
                    )}
                    disableTypography
                    subheader={(
                        <Box
                            display="flex"
                            alignItems="center"
                        >
                            <AccessTimeIcon fontSize="small" />
                            <Typography
                                variant="caption"
                                color="textSecondary"
                                className={classes.date}
                            >
                                {DateHelper.getMoment(post.createdAt).fromNow()}
                            </Typography>

                            {isUserPost ? (
                                <IconButton onClick={handleMenuOpen} ref={moreRef} className={classes.deletePostButton}>
                                    <SvgIcon fontSize="small">
                                        <MoreHorizontal />
                                    </SvgIcon>
                                </IconButton>

                            ) : ''}
                            {isUserPost ? (
                                <Menu
                                    anchorEl={moreRef.current}
                                    keepMounted
                                    elevation={1}
                                    onClose={handleMenuClose}
                                    open={openMenu}
                                >
                                    {getMenuItems()}
                                </Menu>
                            ) : ''
                            }
                        </Box>
                    )}
                    title={(
                        <Box display="flex">
                            <NextLink href={`profiles/${post.author.profileID}`}>
                                <Link
                                    color="textPrimary"
                                    variant="h6"
                                >
                                    {post.author.name}
                                </Link>
                            </NextLink>

                        </Box>
                    )}
                />
                <Box px={3} pb={2}>
                    <Typography
                        variant="body1"
                        color="textPrimary"
                    >
                        {post.message}
                    </Typography>
                    {post.media && post.media.url ? (
                        <Box mt={2}>
                            <CardActionArea onClick={() => {
                                if (debounce === true) return
                                const url = post.media ? post.media.url : undefined
                                setOpenedFile(url)
                            }}>
                                <CardMedia
                                    className={classes.media}
                                    image={post.media.url}
                                />
                            </CardActionArea>
                        </Box>
                    ) : ''}
                    {comments.length > 0 ?
                        <Box my={2}>
                            <Divider />
                        </Box> : ''}
                    {comments.map((comment, index) => (
                        <Comment
                            comment={comment}
                            key={index}
                        />
                    ))}
                    <Box my={2}>
                        <Divider />
                    </Box>
                    <CommentAdd postID={post.postID} level={0} parent={false} />
                </Box>
            </Card>
            {openedFile && (
                <Lightbox
                    hideDownload={true}
                    hideZoom={true}
                    large={openedFile}
                    onClose={() => {
                        setDebounce(true)
                        setTimeout(() => {
                            setDebounce(false)
                        }, 200)
                        setOpenedFile(undefined)
                    }
                    }
                />
            )}
        </>
    );
}

PostCard.propTypes = {
    className: PropTypes.string,
    post: PropTypes.object.isRequired
};

export default PostCard;
