import { Box, Container, makeStyles } from "@material-ui/core";
import { GridColDef, GridPageChangeParams, GridValueFormatterParams } from "@material-ui/data-grid";
import { ISubscription, IUser, IUserFilters, IUserRetrievedFirebase } from '@warnster/shared-interfaces';
import {
  DateHelper,
  UserDaoClient, UserHelper
} from "@warnster/shared-library";
import firebase from 'firebase';
import React, { useEffect, useState } from "react";
import Header from "../Layout/Header";
import Page from "../Page";
import DataTable from "../Table";
import { renderCellExpand, YesNoFormatter } from "../table/ColumnFormatters";

const useStyles = makeStyles((theme: any) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
}));

interface ITableUser extends IUser {
  id: number,
  subscription: ISubscription | null
}

function UserListView() {
  const classes = useStyles();
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0)
  const [gridColumns, setGridColumns] = useState<GridColDef[]>([
    {
      field: 'uid',
      renderCell: renderCellExpand
    },
    {
      field: 'email',
      headerName: 'Email',
      flex: 1,
    },
    {
      field: 'phone',
      headerName: 'Phone',
      renderCell: renderCellExpand
    },
    {
      field: 'isAdmin',
      headerName: 'Admin',
      width: 100,
      align: 'center',
      renderCell: YesNoFormatter
    },
    {
      field: 'profileComplete',
      headerName: "Complete",
      width: 150,
      align: 'center',
      renderCell: YesNoFormatter
    },
    {
      field: 'subscription',
      headerName: "Tier",
      width: 150,
      align: 'center',
      renderCell: (params) => {
        if (!params.value) {
          return (<span>Free</span>)
        }
        const subscription = params.value as ISubscription
        return (
          <strong>{subscription.items[0].price.product.name} | {subscription.items[0].plan.active ? "active" : "cancelled"}</strong>
        )
      }
    },
    {
      field: 'stripeLink',
      headerName: 'Stripe',
      width: 150,
      renderCell: (params) => {
        const stripeID = params.getValue('stripeId')
        const stripeLink = params.value
        if (stripeLink) {
          return (
            <a href={stripeLink as string}>{stripeID}</a>
          )
        } else {
          return (<span>NA</span>)
        }
      }
    },
    {
      field: 'createdAt', headerName: 'Joined',
      width: 100,
      valueFormatter: (params: GridValueFormatterParams) =>
        DateHelper.formatShortAppDate(DateHelper.getMoment(params.value as Date)),
    }
  ])
  const [isLoading, setIsLoading] = useState(true)
  const [startAfter, setStartAfter] = useState<firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>>()
  const [startAt, setStartAt] = useState<firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>>()
  const userDao = new UserDaoClient();
  const [userFilters, setUserFilters] = useState<IUserFilters>({
    limit,
    orderColumn: 'createdAt',
    order: 'desc'
  })
  const [users, setUsers] = useState<ITableUser[]>([])

  const onPageChange = (params: GridPageChangeParams) => {
    const newPage = params.page;
    if (newPage > page) {
      getUsers({ ...userFilters, startAt: null, startAfter: startAfter })
    } else {
      getUsers({ ...userFilters, startAt: startAt, startAfter: null })
    }
    setPage(newPage)
  }

  const getUsers = async (userFilters: IUserFilters) => {
    setIsLoading(true)
    const snapshot = await userDao.getPaginatedUsers(userFilters)
    const tmpUsers: ITableUser[] = []

    for (let j = 0; j < snapshot.docs.length; j++) {
      const doc = snapshot.docs[j]
      const subSnapshot = await doc.ref.collection('subscriptions').get()
      let subscription: ISubscription | null = null;
      for (let i = 0; i < subSnapshot.docs.length; i++) {
        subscription = subSnapshot.docs[i].data() as ISubscription;
        break
      }

      const firebaseUser = doc.data() as IUserRetrievedFirebase
      const tableUser: ITableUser = { ...UserHelper.transformFirebaseUser(firebaseUser), id: j, subscription: subscription }
      tmpUsers.push(tableUser)
    }
    const startAfter = snapshot.docs[snapshot.docs.length - 1]
    setStartAfter(startAfter)
    setStartAt(snapshot.docs[0])
    setUsers(tmpUsers)
    setIsLoading(false)
  }


  useEffect(() => {
    getUsers(userFilters)
  }, [])

  return (
    <Page className={classes.root} title="Customer List">
      <Container maxWidth={false}>
        <Header pageTitle="Users" />
        <Box mt={3}>
          <DataTable columns={gridColumns} rows={users} loading={isLoading} pageSize={limit} rowCount={50} onPageChange={onPageChange}></DataTable>
        </Box>
      </Container>
    </Page>
  );
}

export default UserListView;
