import { AccountActions, IAppState, UserDaoClient } from "@warnster/shared-library";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Error404View from "../../pages/404";
import {
  deleteAuthCookie,
  isLoggedIn,
  setAuthCookieValue
} from "../store/persistant/cookieStore";

const unauthorisedRoutes = [
  "/",
  "/login",
  "/forgotten-password",
  "/reset-password",
  "/email-verification",
  "/action-handler",
];

/**
 * This component handles checking the firebase authentication jwt token. Every page load will refresh the token.
 * The token expires in 1 hour. The user token is reset in the cookie
 * @param WrappedComponent
 */
export const withAuth = (WrappedComponent: any) => {
  const Auth = (...props: any) => {
    const router = useRouter();
    const { user, profile } = useSelector((state: IAppState) => state.account);
    const dispatch = useDispatch();
    const hasAuthCookie = isLoggedIn();
    const userDao = new UserDaoClient();

    useEffect(() => {
      userDao.onFirebaseIdTokenChanged(async (user) => {
        if (user) {
          try {
            const token = await user.getIdToken();
            if (hasAuthCookie) {
              setAuthCookieValue("token", token);
            }
          } catch (err) {
            console.error(err);
          }
        } else {
          console.error(
            "User not signed in, loggout the user",
            router.pathname
          );
          if (!unauthorisedRoutes.includes(router.pathname)) {
            dispatch(AccountActions.logout());
            deleteAuthCookie();
            router.push("/login");
          }
        }
      });
    }, []);

    let allowed = true;
    if (router.pathname.startsWith("/admin") && !user.isAdmin) {
      allowed = false;
    }
    //check user is admin
    //redirect to page if not admin but accessing admin routes

    //Redirect uncomplete profiles back to profile pending page
    if (
      hasAuthCookie &&
      profile.isComplete === false &&
      router.pathname !== "/setup-steps"
    ) {
      router.push("/setup-steps");
      return <div></div>;
    }

    if (!allowed) {
      return <Error404View></Error404View>;
    }
    return <WrappedComponent {...props} />;
  };

  return Auth;
};
