/* eslint-disable react/no-array-index-key */
import {
  Backdrop,
  Box,
  Button,

  CircularProgress,

  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,


  makeStyles,
  Typography
} from '@material-ui/core';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { IFileDisplay, usePrevious, UtilHelper } from '@warnster/shared-library';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import PerfectScrollbar from 'react-perfect-scrollbar';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative'
  },
  crossButton: {
    color: theme.palette.warning.main
  },
  dropZone: {
    border: `1px dashed ${theme.palette.divider}`,
    padding: theme.spacing(6),
    outline: 'none',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      opacity: 0.5,
      cursor: 'pointer'
    }
  },
  dragActive: {
    backgroundColor: theme.palette.action.active,
    opacity: 0.5
  },
  fileImage: {
    maxWidth: 100,
    maxHeight: 100,
    marginRight: theme.spacing(1)
  },
  image: {
    width: 130
  },
  info: {
    marginTop: theme.spacing(1)
  },
  list: {
    maxHeight: 320
  },
  actions: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'flex-end',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  backdrop: {

    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));



interface IFileDropzoneProps {
  className?: string,
  uploadFiles: (files: File[]) => void,
  isSubmitting: boolean
}

function FilesDropzone({ uploadFiles, className, isSubmitting, ...rest }: IFileDropzoneProps) {
  const classes = useStyles();
  const [files, setFiles] = useState<IFileDisplay[]>([]);
  const previousIsSubmitting = usePrevious(isSubmitting)
  const handleDrop = useCallback(async (acceptedFiles: File[]) => {
    let displayFiles: IFileDisplay[] = [];
    for (let i = 0; i < acceptedFiles.length; i++) {
      const file = acceptedFiles[i];
      const base64 = await UtilHelper.getBase64(file);
      const displayFile: IFileDisplay = file;
      if (base64) {
        displayFile.url = base64 as string;
      }
      displayFiles.push(displayFile)
    }
    updateFiles([...files, ...displayFiles])
  }, []);

  const handleRemoveAll = () => {
    updateFiles([]);
  };

  const updateFiles = (files: IFileDisplay[]) => {
    setFiles(files)
  }

  useEffect(() => {
    console.log({ previousIsSubmitting, isSubmitting })
    if (previousIsSubmitting && !isSubmitting) {
      updateFiles([])
    }
  }, [isSubmitting])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop
  });

  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Backdrop className={classes.backdrop} open={isSubmitting}>
        <CircularProgress color="primary" />
      </Backdrop>
      <div
        className={clsx({
          [classes.dropZone]: true,
          [classes.dragActive]: isDragActive
        })}
        {...getRootProps()}
      >
        <input {...getInputProps()} />
        <div>
          <img
            alt="Select file"
            className={classes.image}
            src="/static/images/undraw_add_file2_gvbb.svg"
          />
        </div>
        <div>
          <Typography
            gutterBottom
            variant="h3"
          >
            Select files
          </Typography>
          <Box mt={2}>
            <Typography
              color="textPrimary"
              variant="body1"
            >
              Drop files here or click
              {' '}
              <Link underline="always">browse</Link>
              {' '}
              thorough your machine
            </Typography>
          </Box>
        </div>
      </div>
      {files.length > 0 && (
        <>
          <PerfectScrollbar options={{ suppressScrollX: true }}>
            <List className={classes.list}>
              {files.map((file, i) => {
                return (
                  <ListItem
                    divider={i < files.length - 1}
                    key={i}
                  >
                    <ListItemIcon>
                      {file.url ?
                        <img src={file.url} alt={file.name} className={classes.fileImage} /> :
                        <FileCopyIcon />
                      }

                    </ListItemIcon>
                    <ListItemText
                      primary={file.name}
                      primaryTypographyProps={{ variant: 'h5' }}
                      secondary={UtilHelper.bytesToSize(file.size)}
                    />
                  </ListItem>
                )
              })}
            </List>
          </PerfectScrollbar>
          <div className={classes.actions}>
            <Button
              onClick={handleRemoveAll}
              size="small"
            >
              Remove all
            </Button>
            <Button
              color="secondary"
              size="small"
              variant="contained"
              onClick={() => uploadFiles(files)}
            >
              Upload files
            </Button>
          </div>
        </>
      )}
    </div>
  );
}

FilesDropzone.propTypes = {
  className: PropTypes.string
};

export default FilesDropzone;
