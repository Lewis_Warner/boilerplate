
import { Box, Container, makeStyles, Theme } from "@material-ui/core";
import { IUseProfiles } from '@warnster/shared-interfaces';
import { IAppState, useProfiles } from "@warnster/shared-library";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import Header from "../src/components/Layout/Header";
import Page from "../src/components/Page";
import Filter from "../src/containers/ProjectBrowseView/Filter";
import ProfileResults from "../src/containers/ProjectBrowseView/ProfileResults";
import withDashboardLayout from "../src/layout/dashboard";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        //@ts-ignore
        backgroundColor: theme.palette.background.dark,
        minHeight: '100%',
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3)
    }
}));

const ProfilesPage = () => {

    const classes = useStyles()
    const { profile, user } = useSelector((state: IAppState) => state.account)
    const [profileFilters, setProfileFilters] = useState<IUseProfiles>({
        filters: {
        },
        limit: 5,
        order: "desc",
        orderColumn: "createdAt",
        trigger: false
    })
    const { profiles, isLoading } = useProfiles(profileFilters)

    return (
        <Page
            className={classes.root}
            title="Profiles"
        >
            <Container maxWidth="lg">
                <Header pageTitle="Profiles" />
                <Box mt={3}>
                    <Filter />
                </Box>
                <Box mt={6}>
                    <ProfileResults profiles={profiles} />
                </Box>
            </Container>
        </Page>
    )
};

export default withDashboardLayout(ProfilesPage);