
import { Box, Container, Divider, makeStyles, Tab, Tabs, Theme, Typography } from "@material-ui/core";
import { IProfile } from "@warnster/shared-interfaces";
import React, { useState } from "react";
import { MapPin } from "react-feather";
import safeJsonStringify from "safe-json-stringify";
import Page from "../../src/components/Page";
import PhotoDisplay from "../../src/containers/ProfileView/PhotoDisplay";
import ProfileDisplay from "../../src/containers/ProfileView/ProfileDisplay";
import ProfileHeader from "../../src/containers/ProfileView/ProfileHeader";
import { ProfileDaoAdmin } from "../../src/daos/profile_admin_dao";
import withDashboardLayout from "../../src/layout/dashboard";

const tabs = [
    { value: 'Profile', label: 'Posts' },
    { value: 'Photo', label: 'Photos' },
];

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        //@ts-ignore
        backgroundColor: theme.palette.background.dark,
        minHeight: '100%',
    }
}));

export async function getServerSideProps(context: any) {
    const profileID = context.query.profileID as string
    const profileDao = new ProfileDaoAdmin()
    let profile = await profileDao.getProfileByID(profileID)
    profile = JSON.parse(safeJsonStringify(profile ? profile : {}))
    return {
        props: {
            profile
        }
    }
}

export const Profile = (props: { profile: IProfile }[]) => {
    const { profile } = props[0]
    const classes = useStyles()
    const [currentTab, setCurrentTab] = useState('Profile');

    const handleTabsChange = (event: any, value: string) => {
        setCurrentTab(value);
    };

    return (
        <Page
            className={classes.root}
            title={profile.name}
        >
            <ProfileHeader profile={profile} />
            <Container maxWidth="lg">
                <Box mt={3}>
                    <Typography
                        variant="h5"
                        color="textPrimary"
                    >
                        this is going to be the description is can be great or terrible does the text qra
            </Typography>

                    <Typography
                        variant="h5"
                        color="textSecondary"
                    >
                        25 | Female | Findom <br />
                        <MapPin style={{ height: '16px' }}></MapPin> United Kingdom
            </Typography>
                </Box>
            </Container>
            <Container maxWidth="lg">
                <Box mt={1}>
                    <Tabs
                        onChange={handleTabsChange}
                        scrollButtons="auto"
                        value={currentTab}
                        textColor="secondary"
                        variant="scrollable"
                    >
                        {tabs.map((tab) => (
                            <Tab
                                key={tab.value}
                                label={tab.label}
                                value={tab.value}
                            />
                        ))}
                    </Tabs>
                </Box>
                <Divider />
                <Box
                    py={3}
                    pb={6}
                >
                    {currentTab === 'Profile' && <ProfileDisplay profile={profile} />}
                    {currentTab === 'Photo' && <PhotoDisplay profile={profile} />}
                </Box>
            </Container>
        </Page>
    )
}

export default withDashboardLayout(Profile)