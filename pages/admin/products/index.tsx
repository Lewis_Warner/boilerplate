import { Box, Button, Container, Dialog, DialogContent, DialogTitle, makeStyles, Theme } from "@material-ui/core";
import { GridColDef, GridRowsProp } from '@material-ui/data-grid';
import { IProductDetails } from '@warnster/shared-interfaces';
import { Stripe } from "@warnster/shared-library";
import React, { ReactElement, useEffect, useState } from "react";
import Page from "../../../src/components/Page";
import CreateEditForm from "../../../src/components/Product/CreateEditForm";
import DataTable from "../../../src/components/Table";
import { renderCellExpand, YesNoFormatter } from "../../../src/components/table/ColumnFormatters";
import withDashboardLayout from "../../../src/layout/dashboard";




const useStyles = makeStyles((theme: Theme) => ({
  root: {
    //@ts-ignore
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
  productImage: {
    height: 48,
    width: 48
  },
}));



const ProductsPage: React.FC = (): ReactElement => {
  const classes = useStyles();
  const [gridColumns, setGridColumns] = useState<GridColDef[]>([
    {
      field: 'images',
      headerName: 'Image',
      width: 100,
      renderCell: ((params) => {
        const images: string[] = params.value as string[];
        if (images.length > 0) {
          return (<img
            alt="Product"
            className={classes.productImage}
            src={images[0]}
          />)
        } else {
          return (<div></div>)
        }
      })
    },
    {
      field: 'name', width: 100, headerName: 'Name',
      renderCell: renderCellExpand
    },
    {
      field: 'active',
      headerName: 'Active',
      align: 'center',
      renderCell: YesNoFormatter
    },
    {
      field: 'hasUsagePlan',
      headerName: 'Usage',
      width: 100,
      align: 'center',
      renderCell: YesNoFormatter
    },
    {
      field: 'description',
      headerName: 'Description',
      flex: 1,
      renderCell: renderCellExpand
    },
    { field: 'role', headerName: 'Role' },
    { field: 'messages', headerName: 'Messages', flex: 1 },
    { field: 'profileViews', headerName: 'Profile Views', flex: 1 },
    {
      field: 'id', headerName: 'Actions',
      width: 120,
      renderCell: (params) => {
        const usage = params.getValue('hasUsagePlan')
        if (usage) {
          return (
            <Button variant="contained" onClick={() => {
              setOpen(true),
                setSelectedProduct(params.row as IProductDetails)
            }}>Edit</Button>
          )
        }
        return (
          <Button variant="outlined" onClick={() => {
            setOpen(true),
              setSelectedProduct(params.row as IProductDetails)
          }}>Create</Button>
        )

      }
    }
  ])
  const [gridRows, setGridRows] = useState<GridRowsProp>([])
  const [isLoading, setIsLoading] = useState(true);
  const [selectedProduct, setSelectedProduct] = useState<IProductDetails>()
  const [open, setOpen] = useState(false)

  const init = async () => {
    //get all products and productUsage
    const stripe = new Stripe()
    const products = await stripe.getProductDetails();
    setGridRows(products)
    setIsLoading(false);
  }

  useEffect(() => {
    init();
  }, []);

  return (
    <Page className={classes.root} title="Products">
      <Container maxWidth={false}>
        <Box mt={3}>
          <DataTable rowCount={5} onPageChange={(params) => { }} columns={gridColumns} rows={gridRows} loading={isLoading} pageSize={5}></DataTable>
        </Box>
        <Dialog
          open={open}
          onClose={() => { setOpen(false) }}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth='md'
        >
          <DialogTitle id="form-dialog-title">Product Create and Edit Form</DialogTitle>
          <DialogContent>
            {selectedProduct ?
              <CreateEditForm productDetails={selectedProduct}></CreateEditForm>
              : ''}
          </DialogContent>
        </Dialog>
      </Container>
    </Page>
  )
};

export default withDashboardLayout(ProductsPage);
