import { ReactElement } from "react";
import UserListView from "../../src/components/UserListView";
import withDashboardLayout from "../../src/layout/dashboard";

const UserAdminPage: React.FC = (): ReactElement => {
  return <UserListView></UserListView>;
};

export default withDashboardLayout(UserAdminPage);
