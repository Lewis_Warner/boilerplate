import { IAppState } from "@warnster/shared-library";
import React from "react";
import { useSelector } from "react-redux";
import { isLoggedIn } from "../src/store/persistant/cookieStore";
import UserAdminPage from "./admin/users";
import LoginView from "./login";
import ProfilesPage from './profiles';

const Index = () => {
  const { user } = useSelector((state: IAppState) => state.account);
  return !isLoggedIn() ? (
    <LoginView></LoginView>
  ) : user.isAdmin ? (
    <UserAdminPage></UserAdminPage>
  ) : (
    <ProfilesPage></ProfilesPage>
  );
};

export default Index;
