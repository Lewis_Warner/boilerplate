import {
  Box,
  Card,
  CardContent,
  Container,
  Divider,
  Link,
  makeStyles,
  Typography,
} from "@material-ui/core";
import NextLink from "next/link";
import PropTypes from "prop-types";
import React from "react";
import ForgottenPasswordForm from "../src/containers/forgotten-password";

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: "center",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    minHeight: "100%",
    flexDirection: "column",
    paddingBottom: 80,
    paddingTop: 80,
  },
}));

function ForgottenPasswordPage({ className, ...rest }: { className: string }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container maxWidth="sm">
        <Card>
          <CardContent>
            <Typography gutterBottom variant="h2" color="textPrimary">
              Forgotten Password
            </Typography>
            <Typography variant="subtitle1">
              Enter your email address and instruction to reset your password
              will be sent
            </Typography>
            <Box mt={3}>
              <ForgottenPasswordForm />
            </Box>
            <Box my={2}>
              <Divider />
            </Box>

            <NextLink href="/login">
              <Link href="#" variant="body2" color="textSecondary">
                Have an account?
              </Link>
            </NextLink>
          </CardContent>
        </Card>
      </Container>
    </div>
  );
}

ForgottenPasswordPage.propTypes = {
  className: PropTypes.string,
};

export default ForgottenPasswordPage;
