import {
  Box,
  Card,
  CardContent,
  Container,
  Divider,
  Link,
  makeStyles,
  Typography,
} from "@material-ui/core";
import NextLink from "next/link";
import PropTypes from "prop-types";
import React from "react";
import ResetPasswordForm from "../src/containers/reset-password";

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: "center",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    minHeight: "100%",
    flexDirection: "column",
    paddingBottom: 80,
    paddingTop: 80,
  },
}));

function ResetPasswordPage({ className, ...rest }: { className?: string }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container maxWidth="sm">
        <Card>
          <CardContent>
            <Typography gutterBottom variant="h2" color="textPrimary">
              Reset Password
            </Typography>
            <Typography variant="subtitle1">Enter your new password</Typography>
            <Box mt={3}>
              <ResetPasswordForm />
            </Box>
            <Box my={2}>
              <Divider />
            </Box>

            <NextLink href="/login">
              <Link href="#" variant="body2" color="textSecondary">
                Login
              </Link>
            </NextLink>
          </CardContent>
        </Card>
      </Container>
    </div>
  );
}

ResetPasswordPage.propTypes = {
  className: PropTypes.string,
};

export default ResetPasswordPage;
