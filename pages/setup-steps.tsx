import {
    Avatar,
    Box,
    Button,
    Card,
    CardContent,
    colors,
    Container,
    Grid,
    makeStyles,
    Paper,
    Step,
    StepLabel,
    Stepper,
    Typography
} from "@material-ui/core";
import { AccountActions, IAppState, ProfileDaoClient, useFirebaseUser, UserDaoClient, UserHelper } from "@warnster/shared-library";
import NextLink from 'next/link';
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import {
    Icon,
    Mail as EmailIcon,
    Star as StarIcon,
    User as UserIcon
} from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import Page from "../src/components/Page";
import EmailConfirmation from "../src/components/ProfilePending/EmailConfirmation";
import ProfileDetails from "../src/components/ProfilePending/ProjectDetails";
import CustomStepConnector from "../src/components/Stepper/CustomStepConnector";
import CustomStepIcon from "../src/components/Stepper/CustomStepIcon";
import { deleteAuthCookie } from "../src/store/persistant/cookieStore";

export interface IStep {
    label: string,
    icon: Icon
}

const steps: IStep[] = [
    {
        label: "Confirm Email",
        icon: EmailIcon,
    },
    {
        label: "Your Details",
        icon: UserIcon,
    },
];

const useStyles = makeStyles((theme: any) => ({
    root: {
        backgroundColor: theme.palette.background.dark,
        minHeight: "100%",
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
    },
    avatar: {
        backgroundColor: colors.red[600],
    },
}));


const SetupSteps = () => {
    const dispatch = useDispatch()
    const classes = useStyles()
    const { user, profile } = useSelector((state: IAppState) => state.account)
    const { firebaseUser, isLoading } = useFirebaseUser()
    const [activeStep, setActiveStep] = useState(0);
    const userDao = new UserDaoClient();
    const profileDao = new ProfileDaoClient()
    const router = useRouter()
    const { enqueueSnackbar } = useSnackbar();
    const [completed, setCompleted] = useState(false);
    let emailVerifiedListenerInterval: NodeJS.Timeout;
    let userListenerInterval: NodeJS.Timeout;

    let reduxComplete = user.uid !== "";


    useEffect(() => {
        if (!reduxComplete && firebaseUser) {
            let userFound = false;
            let profileFound = false;
            userListenerInterval = setInterval(async () => {
                if (!userFound) {
                    //Tries to get user if it isn't in redux after first ever login
                    const user = await userDao.findUserByID(firebaseUser.uid)
                    if (user) {
                        userFound = true;
                        dispatch(AccountActions.setUser(user));
                    }
                }
                if (!profileFound) {
                    try {
                        const profile = await profileDao.getProfileByUserID(firebaseUser.uid);
                        if (profile) {
                            dispatch(AccountActions.setProfile(profile));
                            profileFound = true
                        }
                    } catch (err) {
                        console.error(err)
                    }
                }
                if (profileFound && userFound) {
                    if (userListenerInterval) {
                        clearInterval(userListenerInterval)
                    }
                    reduxComplete = true
                }
            }, 3000)
            //start interval to fetch user.
        }
        return () => {
            if (userListenerInterval) {
                console.log('clearing user interval')
                clearInterval(userListenerInterval)
            }
        }
    }, [firebaseUser])

    useEffect(() => {
        if (!firebaseUser) return
        //reload to get latest on redirect
        firebaseUser.reload();
        const isFbOrTw = UserHelper.isFacebookOrTwitter(firebaseUser)
        if (firebaseUser.emailVerified === false && isFbOrTw === false) {
            //set the tab to email verified
            //setup listener for emailVerified
            emailVerifiedListenerInterval = setInterval(async () => {
                await firebaseUser.reload();
                if (firebaseUser.emailVerified === true && emailVerifiedListenerInterval) {
                    clearInterval(emailVerifiedListenerInterval);
                    setActiveStep(1);
                }
            }, 3000)
        } else {
            setActiveStep(1);
            //set tab to complete profile
        }
        return () => {
            if (emailVerifiedListenerInterval) {
                console.log('clearing email listener')
                clearInterval(emailVerifiedListenerInterval)
            }
        }
    }, [firebaseUser])

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const logoutHandler = () => {
        router.push("/login");
        dispatch(AccountActions.logout());
        deleteAuthCookie();
    };

    const handleComplete = async () => {
        //todo: add user to redux
        setCompleted(true);
        //Refresh token to get profileID claims
        const userDao = new UserDaoClient()
        const user = await userDao.getCurrentUser()
        if (user) {
            const token = await user.getIdToken(true)
        }
    };

    const resendVerificationEmail = async () => {
        if (firebaseUser) {
            try {
                await firebaseUser.sendEmailVerification();
                enqueueSnackbar("Verification Email Sent", {
                    variant: "success",
                    anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                    },
                    autoHideDuration: 3000,
                    persist: false,
                });
            } catch (err) {
                console.log({ err });
                enqueueSnackbar("Error Sending Email", {
                    variant: "error",
                    anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                    },
                    autoHideDuration: 3000,
                });
            }
        }
    };

    return (
        <Page className={classes.root} title="Project Create">
            <Container maxWidth="lg">
                <Box mb={3} display="flex">
                    <Typography variant="h3" color="textPrimary">
                        New User Setup
              </Typography>

                    <Box flexGrow={1} />
                    <Button variant="outlined" onClick={() => logoutHandler()} size="large">
                        Logout
              </Button>
                </Box>
                {!completed && !user.profileComplete ? (
                    <Paper>
                        <Grid container>
                            <Grid item xs={12} md={3}>
                                <Stepper
                                    activeStep={activeStep}
                                    connector={<CustomStepConnector />}
                                    orientation="vertical"
                                    component={Box}
                                    //@ts-ignore props not added to component library
                                    bgcolor="transparent"
                                >
                                    {steps.map((step, index) => (
                                        <Step key={step.label}>
                                            <StepLabel StepIconComponent={CustomStepIcon} StepIconProps={{ icon: steps[index].icon }}>
                                                {step.label}
                                            </StepLabel>
                                        </Step>
                                    ))}
                                </Stepper>
                            </Grid>
                            <Grid item xs={12} md={9}>
                                <Box p={3}>
                                    {activeStep === 0 && (
                                        <EmailConfirmation onResendMail={resendVerificationEmail} />
                                    )}
                                    {activeStep === 1 && (
                                        <ProfileDetails
                                            onNext={handleNext}
                                            onComplete={handleComplete}
                                        />
                                    )}
                                </Box>
                            </Grid>
                        </Grid>
                    </Paper>
                ) : (
                    <Card>
                        <CardContent>
                            <Box maxWidth={450} mx="auto">
                                <Box display="flex" justifyContent="center">
                                    <Avatar className={classes.avatar}>
                                        <StarIcon />
                                    </Avatar>
                                </Box>
                                <Box mt={2}>
                                    <Typography variant="h3" color="textPrimary" align="center">
                                        You are all done!
                      </Typography>
                                </Box>
                                <Box mt={2}>
                                    <Typography
                                        variant="subtitle1"
                                        color="textSecondary"
                                        align="center"
                                    >
                                        Congratulations on completing your profile. You can now
                                        continue to the site.
                      </Typography>
                                </Box>
                                <Box mt={2} display="flex" justifyContent="center">
                                    <NextLink href="/" passHref>
                                        <Button variant="contained" color="secondary">
                                            Continue
                      </Button></NextLink>
                                </Box>
                            </Box>
                        </CardContent>
                    </Card>
                )}
            </Container>
        </Page>
    );
}

export default SetupSteps