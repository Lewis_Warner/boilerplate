import { ReactElement } from "react";
import ChatView from "../../src/components/ChatView";
import withDashboardLayout from "../../src/layout/dashboard";

const UserPage: React.FC = (): ReactElement => {
  return <ChatView></ChatView>;
};

export default withDashboardLayout(UserPage);
