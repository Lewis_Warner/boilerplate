import { ReactElement } from "react";
import ChatView from "../../src/components/ChatView";
import withDashboardLayout from "../../src/layout/dashboard";

const ChatPage: React.FC = (): ReactElement => {
  return <ChatView hasQueryParam={false}></ChatView>;
};

export default withDashboardLayout(ChatPage);
