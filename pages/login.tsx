import {
  Avatar,

  Box,

  Card,
  CardContent,
  CardMedia,



  colors, Container,



  Divider,
  Link,


  makeStyles, Typography
} from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import NextLink from "next/link";
import React from "react";
import FirebaseAuth from "../src/components/FirebaseAuthOG";
import Logo from "../src/components/Logo";

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: "center",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    minHeight: "100%",
    flexDirection: "column",
    paddingBottom: 80,
    paddingTop: 80,
  },
  backButton: {
    marginLeft: theme.spacing(2),
  },
  card: {
    overflow: "visible",
    display: "flex",
    position: "relative",
    "& > *": {
      flexGrow: 1,
      flexBasis: "50%",
      width: "50%",
    },
  },
  content: {
    padding: theme.spacing(8, 4, 3, 4),
  },
  icon: {
    backgroundColor: colors.green[500],
    color: theme.palette.common.white,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    position: "absolute",
    top: -32,
    left: theme.spacing(3),
    height: 64,
    width: 64,
  },
  media: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    padding: theme.spacing(3),
    color: theme.palette.common.white,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));

function LoginView() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Box mb={8} display="flex" alignItems="center">
          <Link href="/">
            <Logo />
          </Link>
          {/*
          <Link href="/">
            <Button className={classes.backButton}>Back to home</Button>
          </Link>
          */}
        </Box>
        <Card className={classes.card}>
          <CardContent className={classes.content}>
            <Avatar className={classes.icon}>
              <LockIcon fontSize="large" />
            </Avatar>
            <Typography variant="h2" color="textPrimary">
              Sign in
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              Choose your prefered signin method
            </Typography>
            <Box mt={3}>
              <FirebaseAuth />
            </Box>
            <Box my={2}>
              <Divider />
            </Box>
            <NextLink href={"/forgotten-password"}>
              <Link href="#" variant="body2" color="textSecondary">
                Forgotten Password?
              </Link>
            </NextLink>
          </CardContent>
          <CardMedia
            className={classes.media}
            image="/static/images/auth.png"
            title="Cover"
          >
            <Typography color="inherit" variant="subtitle1">
              It&apos;s amazing to have all my needs in one place
            </Typography>
            <Box alignItems="center" display="flex" mt={3}>
              <Avatar alt="Person" src="/static/images/avatars/avatar_2.png" />
              <Box ml={3}>
                <Typography color="inherit" variant="body1">
                  Ekaterina Tankova
                </Typography>
                <Typography color="inherit" variant="body2">
                  Manager at inVision
                </Typography>
              </Box>
            </Box>
          </CardMedia>
        </Card>
      </Container>
    </div>
  );
}

export default LoginView;
