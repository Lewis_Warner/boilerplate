import {
    Box,
    Container,
    makeStyles
} from '@material-ui/core';
import { IPostFilter } from '@warnster/shared-interfaces';
import { usePosts } from '@warnster/shared-library';
import React from 'react';
import Header from "../src/components/Layout/Header";
import Page from '../src/components/Page';
import PostAdd from '../src/components/Posts/PostAdd';
import PostCard from '../src/components/Posts/PostCard';
import withDashboardLayout from "../src/layout/dashboard";

const useStyles = makeStyles((theme) => ({
    root: {
        //@ts-ignore
        backgroundColor: theme.palette.background.dark,
        minHeight: '100%',
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3)
    }
}));

function SocialFeedView() {
    const classes = useStyles();
    const postFilter: IPostFilter = {
        limit: 20,
    }
    const { posts } = usePosts(postFilter);

    return (
        <Page
            className={classes.root}
            title="Profiles"
        >
            <Container maxWidth="lg">
                <Header pageTitle="Feed" />
                <Box mt={3}>
                    <PostAdd />
                </Box>
                {
                    posts.map((post, index) => (
                        <Box
                            mt={3}
                            key={index}
                        >
                            <PostCard post={post} />
                        </Box>
                    ))

                }
            </Container>
        </Page>
    );
}

export default withDashboardLayout(SocialFeedView);
