import MomentUtils from "@date-io/moment";
import CssBaseline from "@material-ui/core/CssBaseline";
import {
  createStyles,
  makeStyles,
  ThemeProvider
} from "@material-ui/core/styles";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { AppProps } from "next/app";
import Head from "next/head";
import { useRouter } from "next/router";
import { SnackbarProvider } from "notistack";
import React, { useEffect } from "react";
import "react-perfect-scrollbar/dist/css/styles.css";
import { useStore } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { withAuth } from "../src/components/Auth";
import { Spinner } from "../src/components/Spinner/Spinner";
import BaseWrapper from "../src/containers/base_wrapper";
import { pageview } from "../src/helpers/google_analytics";
import { ISettings } from "../src/interfaces/ui_interfaces";
import { wrapper } from "../src/store/store";
import { createTheme } from "../src/theme/multi-theme";
import { THEMES } from "../src/theme/theme";

function MyApp(props: AppProps) {
  const router = useRouter()
  const useStyles = makeStyles(() =>
    createStyles({
      "@global": {
        "*": {
          boxSizing: "border-box",
          margin: 0,
          padding: 0,
        },
        html: {
          "-webkit-font-smoothing": "antialiased",
          "-moz-osx-font-smoothing": "grayscale",
          height: "100%",
          width: "100%",
        },
        body: {
          height: "100%",
          width: "100%",
        },
        "#__next": {
          height: "100%",
          width: "100%",
        },
      },
    })
  );

  useStyles();
  const { Component, pageProps } = props;

  // @ts-ignore
  const store = useStore((state: any) => state);
  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement!.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      console.log(url)
      pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  const defaultSettings: ISettings = {
    direction: "ltr",
    responsiveFontSizes: true,
    theme: THEMES.LIGHT,
  };

  const AuthWrappedComponent = withAuth(Component);
  return (
    <React.Fragment>
      <Head>
        <title>My page</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>

      <ThemeProvider theme={createTheme(defaultSettings)}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />

        <MuiPickersUtilsProvider utils={MomentUtils}>
          <SnackbarProvider maxSnack={3}>
            <PersistGate
              //@ts-ignore
              persistor={store.__persistor}
              loading={<Spinner type="absolute" />}
            >
              <BaseWrapper>
                <AuthWrappedComponent {...pageProps} />
              </BaseWrapper>
            </PersistGate>
          </SnackbarProvider>
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    </React.Fragment>
  );
}

export default wrapper.withRedux(MyApp);
