import csc, { ICity, ICountry, IState } from 'country-state-city';
import { isArray } from "lodash";
import { NextApiRequest, NextApiResponse } from "next";
import { checkRequestIsAuthorised } from '../../../src/helpers/request_helper';


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const {
        query: { slug },
    } = req;
    let slugArr: string[];
    if (!isArray(slug)) {
        slugArr = [slug];
    } else {
        slugArr = <string[]>slug;
    }
    const url = slugArr.join("/");
    const method = req.method;

    if (url === "country" && method === "GET") {
        const userToken = await checkRequestIsAuthorised(req);
        if (userToken === false) {
            return res.status(401).json({ error: "Invalid token" });
        }
        return await getCountry(req, res);
    }

    if (url === "countries" && method === "GET") {
        const userToken = await checkRequestIsAuthorised(req);
        if (userToken === false) {
            return res.status(401).json({ error: "Invalid token" });
        }
        return await getCountries(req, res);
    }

    if (url === "counties" && method === "GET") {
        const userToken = await checkRequestIsAuthorised(req);
        if (userToken === false) {
            return res.status(401).json({ error: "Invalid token" });
        }
        return await getCounties(req, res);
    }

    if (url === "cities" && method === "GET") {
        const userToken = await checkRequestIsAuthorised(req);
        if (userToken === false) {
            return res.status(401).json({ error: "Invalid token" });
        }
        return await getCities(req, res);
    }

    if (url === "all" && method === "GET") {
        const userToken = await checkRequestIsAuthorised(req);
        if (userToken === false) {
            return res.status(401).json({ error: "Invalid token" });
        }
        return await getAll(req, res);
    }

    return res.status(404);
};

const getAll = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    const countryCode = req.query.countryCode as string
    const countyCode = req.query.countyCode as string
    const cityCode = req.query.cityCode as string
    let country: ICountry | null = null;
    let county: IState | null = null;
    let city: ICity | null = null
    if (countryCode) {
        country = csc.getCountryByCode(countryCode) ?? null
    }
    if (countyCode) {
        const counties = csc.getStatesOfCountry(countryCode)
        county = counties.find((county) => county.isoCode === countyCode) ?? null
    }
    if (cityCode) {
        const cities = csc.getCitiesOfState(countryCode, countyCode)
        city = cities.find((city) => city.name === cityCode) ?? null
    }
    return res.status(200).json({ country, county, city });
}

const getCountry = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    const country = csc.getCountryByCode(req.query.countryCode as string)
    return res.status(200).json(country);
}


const getCountries = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    const countries = csc.getAllCountries()
    return res.status(200).json(countries);
}

const getCounties = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    const countryCode = req.query.countryCode as string
    const counties = csc.getStatesOfCountry(countryCode)
    return res.status(200).json(counties);
}

const getCities = async (
    req: NextApiRequest,
    res: NextApiResponse
) => {
    const countryCode = req.query.countryCode as string
    const stateCode = req.query.countyCode as string
    const cities = csc.getCitiesOfState(countryCode, stateCode)
    return res.status(200).json(cities);
}

export default handler;
