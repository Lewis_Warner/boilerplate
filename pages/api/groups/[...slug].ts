import { IFirebaseJWT, IMessageGroup, IProfile } from "@warnster/shared-interfaces";
import { isArray } from "lodash";
import { NextApiRequest, NextApiResponse } from "next";
import { ChatDaoAdmin } from "../../../src/daos/chat_admin_dao";
import { UserDaoAdmin } from "../../../src/daos/user_admin_dao";

const addUserToGroup = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const { group, profiles }: { group: IMessageGroup, profiles: IProfile[] } = req.body;
  //const updateData = addUsersToGroup(users, group)
  try {
    const chatDao = new ChatDaoAdmin()
    const userDao = new UserDaoAdmin();
    const groupUpdateData = chatDao.addUsersToGroup(profiles, group)
    //loop through users and add into group
    profiles.forEach(profile => {
      userDao.addUserToGroup(group.id, profile.userID)
    })

    //create message,
    const names = profiles.map(p => p.name)
    const message = chatDao.createMessage(`${names.join(', ')} joined the chat`, '', 'system');

    //Send Message
    chatDao.addMessage(message, group.id)
    groupUpdateData.recentMessage = message;
    chatDao.updateGroup(group.id, groupUpdateData);
    return res.status(200).json({});
  } catch (err) {
    return res.status(500).json({ err })
  }
};

const checkRequestIsAuthorised = async (
  req: NextApiRequest
): Promise<IFirebaseJWT | false> => {
  const token = <string>req.headers["x-access-token"];
  try {
    const userDao = new UserDaoAdmin();
    return <IFirebaseJWT>await userDao.verifyIdToken(token);
  } catch (err) {
    return false;
  }
};

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { slug },
  } = req;
  let slugArr: string[];
  if (!isArray(slug)) {
    slugArr = [slug];
  } else {
    slugArr = <string[]>slug;
  }
  const url = slugArr.join("/");
  const method = req.method;

  if (url === "add-group-users" && method === "POST") {
    const userToken = await checkRequestIsAuthorised(req);
    if (userToken === false) {
      return res.status(401).json({ error: "Invalid token" });
    }

    //const chatDao = new ChatDaoClient()
    return await addUserToGroup(req, res);
  }
  return res.status(404);
};

export default handler;
