import { IFirebaseJWT } from "@warnster/shared-interfaces";
import firebase from "firebase-admin";
import { isArray } from "lodash";
import { NextApiRequest, NextApiResponse } from "next";
import { UserDaoAdmin } from "../../../../src/daos/user_admin_dao";
const updateAdminStatus = async (
  req: NextApiRequest,
  res: NextApiResponse,
  userToken: IFirebaseJWT,
  isAdmin: boolean
) => {
  if (userToken.isAdmin === false) {
    return res.status(403);
  }
  try {
    const userID = <string>req.query.uid;

    await firebase.auth().setCustomUserClaims(userID, { isAdmin });
    await firebase.firestore().collection("users").doc(userID).update({
      isAdmin,
    });
    return res.status(200).json({});

  } catch (err) {
    return res.status(500).json({ error: JSON.stringify(err) })
  }
};

const checkRequestIsAuthorised = async (
  req: NextApiRequest
): Promise<IFirebaseJWT | false> => {
  const token = <string>req.headers["x-access-token"];
  try {
    const userDao = new UserDaoAdmin();
    return <IFirebaseJWT>await userDao.verifyIdToken(token);
  } catch (err) {
    return false;
  }
};

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { slug },
  } = req;
  let slugArr: string[];
  if (!isArray(slug)) {
    slugArr = [slug];
  } else {
    slugArr = <string[]>slug;
  }
  const url = slugArr.join("/");
  const method = req.method;
  console.log({ method })
  if (url === "upgrade-admin" && method === "GET") {
    const userToken = await checkRequestIsAuthorised(req);
    console.log({ userToken })
    if (userToken === false) {
      return res.status(401).json({ error: "Invalid token" });
    }

    return await updateAdminStatus(req, res, userToken, true);
  }
  if (url === "downgrade-admin" && method === "GET") {
    const userToken = await checkRequestIsAuthorised(req);
    if (userToken === false) {
      return res.status(401).json({ error: "Invalid token" });
    }
    return await updateAdminStatus(req, res, userToken, false);
  }
  return res.status(404).json({ error: "Route not found" })
};

export default handler;
