import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Divider,
  makeStyles,
  Typography
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { IAppState, UserDaoClient, UtilHelper } from "@warnster/shared-library";
import NextLink from "next/link";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Error404View from "./404";

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: "center",
    // @ts-ignore
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    minHeight: "100%",
    flexDirection: "column",
    paddingBottom: 80,
    paddingTop: 80,
  },
}));

function EmailVerifyPage({ className, ...rest }: { className?: string }) {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [success, setSuccess] = useState<boolean | null>(null);
  const { user } = useSelector((state: IAppState) => state.account);
  const [redirectUrl, setRedirectUrl] = useState<string>("#");

  const init = useCallback(async () => {
    const oobCode = router.query.oobCode as string;
    if (!oobCode) {
      return;
    }
    const url = await UtilHelper.getRedirectUrl(user);
    setRedirectUrl(url);
    const userDao = new UserDaoClient();
    const success = await userDao.verifyEmailAddress(oobCode);
    setSuccess(success);
    setIsLoading(false);
  }, []);

  useEffect(() => {
    init();
  }, [init]);

  if (!router.query?.oobCode) {
    return <Error404View />;
  }

  if (isLoading) {
    return <h1> Loading... </h1>;
  }

  return (
    <div className={classes.root}>
      <Container maxWidth="sm">
        <Card>
          <CardContent>
            <Typography gutterBottom variant="h2" color="textPrimary">
              Email Confirmation
            </Typography>
            <Typography variant="subtitle1">
              {isLoading
                ? "Confirming email address..."
                : success === true
                  ? "Email Confirmed, please continue"
                  : "Error"}
            </Typography>
            {success === false ? (
              <Alert severity="error">
                This link has expired, Please generate another confirmation
                email
              </Alert>
            ) : (
              ""
            )}
            <Box mt={3}>
              {!isLoading && success === true ? (
                <NextLink href={redirectUrl}>
                  <Button variant="contained" color="primary">
                    Continue
                  </Button>
                </NextLink>
              ) : (
                ""
              )}
            </Box>
            <Box my={2}>
              <Divider />
            </Box>
          </CardContent>
        </Card>
      </Container>
    </div>
  );
}

EmailVerifyPage.propTypes = {
  className: PropTypes.string,
};

export default EmailVerifyPage;
