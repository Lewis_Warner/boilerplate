import { useRouter } from "next/router";
import React, { ReactElement } from "react";
import Error404View from "./404";
import EmailVerifyPage from "./email-verification";
import ResetPasswordPage from "./reset-password";

function ActionHandler(): ReactElement {
  const router = useRouter();

  const { mode } = router.query;

  if (!mode) {
    return <Error404View></Error404View>;
  }
  return mode === "resetPassword" ? (
    <ResetPasswordPage />
  ) : mode === "verifyEmail" ? (
    <EmailVerifyPage />
  ) : (
        <div></div>
      );
}

export default ActionHandler;
