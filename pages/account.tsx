import {
    Box,
    Container,
    Divider,
    makeStyles, Tab,
    Tabs
} from '@material-ui/core';
import React, { useState } from 'react';
import Header from '../src/components/Layout/Header';
import Page from '../src/components/Page';
import General from '../src/containers/account/General';
import Photos from '../src/containers/account/Photos';
import Subscription from '../src/containers/account/Subscription';
import withDashboardLayout from '../src/layout/dashboard';

const useStyles = makeStyles((theme) => ({
    root: {
        //@ts-ignore
        backgroundColor: theme.palette.background.dark,
        minHeight: '100%',
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3)
    }
}));

function AccountView() {
    const classes = useStyles();
    const [currentTab, setCurrentTab] = useState('profile');
    const tabs = [
        { value: 'profile', label: 'Profile' },
        { value: 'photos', label: 'Photos' },
        { value: 'subscription', label: 'Subscription' }
    ];

    const handleTabsChange = (event: any, value: string) => {
        setCurrentTab(value);
    };

    return (
        <Page className={classes.root} title="Account">
            <Container maxWidth="lg">
                <Header pageTitle="Account Settings" />
                <Box mt={3}>
                    <Tabs
                        onChange={handleTabsChange}
                        scrollButtons="auto"
                        value={currentTab}
                        variant="scrollable"
                        textColor="secondary"
                        //@ts-ignore
                        className={classes.tabs}
                    >
                        {tabs.map((tab) => (
                            <Tab
                                key={tab.value}
                                label={tab.label}
                                value={tab.value}
                            />
                        ))}
                    </Tabs>
                </Box>
                <Divider />
                <Box mt={3}>
                    {currentTab === 'profile' && <General />}
                    {currentTab === 'photos' && <Photos />}
                    {currentTab === 'subscription' && <Subscription />}
                </Box>
            </Container>
        </Page>
    );
}

export default withDashboardLayout(AccountView);
